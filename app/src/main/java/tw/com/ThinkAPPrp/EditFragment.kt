package tw.com.thinkPower.ThinkAPPrp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_edit.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList
import tw.com.thinkPower.ThinkAPPrp.Model.ProcessDataModel

class EditFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        if (ip.isNotEmpty()) {
            editIPstatus.setImageResource(R.drawable.ic_signal_green)
        }

        button.setOnClickListener {
            processList = mutableListOf(ProcessDataModel("","","1","STEP0001","END","","","","","","","","","","", "","","","","","","","","",""))
            val fragmentManager = activity?.supportFragmentManager
            val fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.fragment_container, StepFragment())
            fragmentTransaction?.commit()
        }

        button2.setOnClickListener {
            val fragmentManager = activity?.supportFragmentManager
            val fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.fragment_container, EditOpenFragment())
            fragmentTransaction?.commit()
        }
    }
}