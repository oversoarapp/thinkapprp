package tw.com.thinkPower.ThinkAPPrp

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.implments.SwipeItemMangerImpl
import kotlinx.android.synthetic.main.fragment_pstart.*
import tw.com.thinkPower.ThinkAPPrp.Adapter.PstartAdapter
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.from
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException

class EditOpenFragment: Fragment() {

    private var command = "0000000FProcessFILEREAD"
    private var nAdapter: PstartAdapter? = null
    private var mHandler:Handler? = null
    private var flag = 1
    private var socket = Socket()
    private var swipeItemManger: SwipeItemMangerImpl? = null
    private var swipeLayout: SwipeLayout? = null
    private var position: Int? = null
    private var processSize = ""
    private var msgSize = ""
    private var process = ""

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_pstart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {
        
        mHandler = Handler()

        val click:(String) -> Unit = {
            command = convertToHex("FILEREAD,ProcessPath/$it")
            connectTask()
        }

        val deleteClick:(String,Int,SwipeLayout,SwipeItemMangerImpl) -> Unit = { name,pos,layout,manger ->
            command = convertToHex("FILEDEL,ProcessPath/$name")
            swipeItemManger = manger
            swipeLayout = layout
            position = pos
            connectTask()
        }

        nAdapter = PstartAdapter(
            context!!,
            true,
            click,
            deleteClick
        )

        pstartRv.layoutManager = LinearLayoutManager(context)
        pstartRv.adapter = nAdapter

        if (GlobalVar.ip.isNotEmpty()) {
            connectTask()
        }

        pstartRefresh.setOnClickListener {
            if (ip.isNotEmpty()) {
                command = "0000000FProcessFILEREAD"
                connectTask()
            }
        }

    }

    private fun convertToHex (data:String):String {
        var zero = ""
        var hex = ""
        hex = if (data.startsWith("FILE")) {
            Integer.toHexString(data.length)
        } else {
            Integer.toHexString(data.length-8)
        }
        var size = 8 - hex.length
        for (i in 0 until size) {
            zero += "0"
        }
        return if (data.startsWith("FILE")) {
            zero+hex+data
        } else {
            zero+hex
        }
    }

    private val reConnectToast = Runnable {
        if(pstartIPstatus != null) {
            pstartIPstatus.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (progressBar2 != null ) {
            ip = ""
            progressBar2.visibility = View.INVISIBLE
            Toast.makeText(context, "連線錯誤，請確認網路正常以及IP位置正確！", Toast.LENGTH_SHORT).show()
            pstartIPstatus.setImageResource(R.drawable.ic_signal_gray)
        }
    }

    private fun connectTask() {
        val t = Thread(readData)
        t.start()
    }

    private fun sendTask() {
        var t = Thread(sendCommand)
        t.start()
    }

    private val readData = Runnable {
        try {
            mHandler?.post {
                if (progressBar2 != null) {
                    progressBar2.visibility = View.VISIBLE
                }
            }
            socket = Socket()
            socket.connect(InetSocketAddress(GlobalVar.ip, 1688), 3000)

            if (socket.isConnected) {
                mHandler?.post {
                    if (pstartIPstatus != null) {
                        pstartIPstatus.setImageResource(R.drawable.ic_signal_green)
                        sendTask()
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <= 2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        } catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <= 2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable {
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand(command)

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
        if (!msg.contains("ERROR") || msg.isNotEmpty()) {
            when (command) {
                "0000000FProcessFILEREAD" -> {
                    if (msg.isNotEmpty()) {
                        var list = msg.substring(8).replace(".json", "").split(",").toMutableList()
                        mHandler?.post {
                            if (progressBar2 != null) {
                                progressBar2.visibility = View.INVISIBLE
                                nAdapter?.updateData(list.sorted().toMutableList())
                            }
                        }
                    }
                }
                else -> {
                    if (command.contains("FILEDEL")) {
                        mHandler?.post {
                            if (progressBar2 != null ) {
                                progressBar2.visibility = View.INVISIBLE
                                nAdapter?.dataList?.removeAt(position ?: 0)
                                nAdapter?.updateData(nAdapter?.dataList!!)
                                swipeItemManger?.removeShownLayouts(swipeLayout)
                                swipeItemManger?.closeAllItems()
                            }
                        }
                    } else {
                        if (msg.length >= 8 ) {
                            process = msg
                            if (processSize.isEmpty()) {
                                //第一次進行
                                processSize = msg.substring(0,8)
                                msgSize = convertToHex(msg)
                            }
                            var processMsg = ""
                            while (processSize != msgSize) {
                                var connect = Connect()
                                connect.sockect(socket)
                                processMsg = connect.sendCommand(command)
                                process += processMsg
                                msgSize = convertToHex(process)
                            }
                            if (!process.contains(processMsg)) {
                                process += processMsg
                            }
                            Log.i("openProcess", process)
                            from = "open"
                            val bundle = Bundle()
                            bundle.putString("ProcessData", process)
                            val fragment = StepFragment()
                            val fragmentManager = activity?.supportFragmentManager
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragment.arguments = bundle
                            fragmentTransaction?.replace(R.id.fragment_container, fragment)
                            fragmentTransaction?.commit()
                        } else {
                            mHandler?.post {
                                AlertDialog.Builder(context)
                                    .setMessage("something wrong,please try again")
                                    .show()
                            }
                        }
                    }
                }
            }
        } else {
            mHandler?.post {
                if (progressBar2 != null) {
                    msg.contains("command")
                    msg = msg.substring(8)
                    progressBar2.visibility = View.INVISIBLE
                    AlertDialog.Builder(context)
                        .setMessage(msg)
                        .show()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mHandler = null
        if (progressBar2 != null && progressBar2.visibility == View.VISIBLE) {
            progressBar2.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (socket.isConnected) {
            var c = Connect()
            c.sockect(socket)
            c.closeIOAndSocket()
        }
    }
}