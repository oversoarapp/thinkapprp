package tw.com.thinkPower.ThinkAPPrp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.view.get
import androidx.fragment.app.FragmentManager
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip


class MainActivity : AppCompatActivity() {

    private var fragmentManager: FragmentManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder(this)
                .setTitle("訊息提示")
                .setMessage("是否離開APP？")
                .setNegativeButton("確定") { dialog, which ->
                    finish()
                }
                .setPositiveButton("取消") { dialog, which ->
                    dialog.cancel()
                }
                .show()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun initView() {

        fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()
        fragmentTransaction?.add(R.id.fragment_container, SetFragment())
        fragmentTransaction?.commit()

        tabLayout3.getTabAt(4)?.select()

        tabLayout3.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {

                if (ip.isNotEmpty()) {
                    tabPerform(tab?.position ?: 0)
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                if (ip.isEmpty() && tab?.position != 4) {
                    tabLayout3.getTabAt(4)?.select()
                } else {
                    tabPerform(tab?.position ?: 0)
                }

            }
        })
    }

    private fun tabPerform(position:Int) {

        when(position) {
            0 -> {
                val f = fragmentManager?.findFragmentById(R.id.fragment_container).toString()
                if (f.contains("SaveProcessFragment") || f.contains("StepFragment")) {
                    android.app.AlertDialog.Builder(this@MainActivity)
                        .setTitle("訊息提示")
                        .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                        .setNegativeButton("確定") { d,w ->
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragmentTransaction?.replace(R.id.fragment_container, HomeFragment())
                            fragmentTransaction?.commit()
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                } else {
                    val fragmentTransaction = fragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(R.id.fragment_container, HomeFragment())
                    fragmentTransaction?.commit()
                }
            }
            1 -> {
                val f = fragmentManager?.findFragmentById(R.id.fragment_container).toString()
                if (f.contains("SaveProcessFragment") || f.contains("StepFragment")) {
                    android.app.AlertDialog.Builder(this@MainActivity)
                        .setTitle("訊息提示")
                        .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                        .setNegativeButton("確定") { d,w ->
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragmentTransaction?.replace(R.id.fragment_container, MstartFragment())
                            fragmentTransaction?.commit()
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                } else {
                    val fragmentTransaction = fragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(R.id.fragment_container, MstartFragment())
                    fragmentTransaction?.commit()
                }
            }
            2 -> {
                val f = fragmentManager?.findFragmentById(R.id.fragment_container).toString()
                if (f.contains("SaveProcessFragment") || f.contains("StepFragment")) {
                    android.app.AlertDialog.Builder(this@MainActivity)
                        .setTitle("訊息提示")
                        .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                        .setNegativeButton("確定") { d,w ->
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragmentTransaction?.replace(R.id.fragment_container, PstartFragment())
                            fragmentTransaction?.commit()
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                } else {
                    val fragmentTransaction = fragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(R.id.fragment_container, PstartFragment())
                    fragmentTransaction?.commit()
                }
            }
            3 -> {
                val f = fragmentManager?.findFragmentById(R.id.fragment_container).toString()
                if (f.contains("SaveProcessFragment") || f.contains("StepFragment")) {
                    android.app.AlertDialog.Builder(this@MainActivity)
                        .setTitle("訊息提示")
                        .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                        .setNegativeButton("確定") { d,w ->
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragmentTransaction?.replace(R.id.fragment_container, EditFragment())
                            fragmentTransaction?.commit()
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                } else {
                    val fragmentTransaction = fragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(R.id.fragment_container, EditFragment())
                    fragmentTransaction?.commit()
                }
            }
            4 -> {
                val f = fragmentManager?.findFragmentById(R.id.fragment_container).toString()
                if (f.contains("SaveProcessFragment") || f.contains("StepFragment")) {
                    android.app.AlertDialog.Builder(this@MainActivity)
                        .setTitle("訊息提示")
                        .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                        .setNegativeButton("確定") { d,w ->
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragmentTransaction?.replace(R.id.fragment_container, SetFragment())
                            fragmentTransaction?.commit()
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                } else {
                    val fragmentTransaction = fragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(R.id.fragment_container, SetFragment())
                    fragmentTransaction?.commit()
                }
            }
            5 -> {
                val f = fragmentManager?.findFragmentById(R.id.fragment_container).toString()
                if (f.contains("SaveProcessFragment") || f.contains("StepFragment")) {
                    android.app.AlertDialog.Builder(this@MainActivity)
                        .setTitle("訊息提示")
                        .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                        .setNegativeButton("確定") { d,w ->
                            val fragmentTransaction = fragmentManager?.beginTransaction()
                            fragmentTransaction?.replace(R.id.fragment_container, InfoFragment())
                            fragmentTransaction?.commit()
                        }
                        .setPositiveButton("取消") {d,w ->
                            d.cancel()
                        }
                        .show()
                } else {
                    val fragmentTransaction = fragmentManager?.beginTransaction()
                    fragmentTransaction?.replace(R.id.fragment_container, InfoFragment())
                    fragmentTransaction?.commit()
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        var c = Connect()
        c.closeIOAndSocket()
    }
}
