package tw.com.thinkPower.ThinkAPPrp
import android.util.Log
import java.io.*
import java.net.Socket
import java.nio.Buffer
import java.nio.ByteBuffer
import java.util.*



class Connect {

    var sock : Socket? = null
    var socketOut:OutputStream ? = null
    var dis:DataInputStream? = null

    fun sockect(mSock:Socket) {
        try {
            sock = mSock
            socketOut = sock?.getOutputStream()
            var ips = sock?.getInputStream()
//            var ipsr = InputStreamReader(ips)
//            writer = PrintWriter(sock?.getOutputStream()!!)
//            reader = BufferedReader(ipsr)
            dis = DataInputStream(ips)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun closeIOAndSocket() {

        try {
            socketOut?.close()
            dis?.close()
            sock?.close()
        } catch (e:IOException) {
            e.printStackTrace()
        }
    }

    fun sendCommand(msg:String):String {
        socketOut?.write(msg.toByteArray(charset("utf-8")))
        socketOut?.flush()
        val buff = ByteArray(3000)
        val len = dis?.read(buff)?:0
        var data = ByteArray(0)
        if (len > 0 ) {
            data = buff.copyOfRange(0, len)
        }
        return String(data)
    }

//    fun readLine():String{
//        return reader?.readLine()?:""
//    }
}