package tw.com.thinkPower.ThinkAPPrp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_set.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip


class SetFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_set, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        val manager = childFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.childFragment, ConnectChildFragment())
        transaction.commit()

        setChildTabLayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        val manager = childFragmentManager
                        val transaction = manager.beginTransaction()
                        transaction.replace(R.id.childFragment, ConnectChildFragment())
                        transaction.commit()
                    }
                    1 -> {
                        if (ip.isNotEmpty()) {
                            val manager = childFragmentManager
                            val transaction = manager.beginTransaction()
                            transaction.replace(R.id.childFragment, ProtectionChildFragment())
                            transaction.commit()
                        } else {
                            setChildTabLayout.getTabAt(0)?.select()
                        }
                    }
                }
            }
        })
    }
}