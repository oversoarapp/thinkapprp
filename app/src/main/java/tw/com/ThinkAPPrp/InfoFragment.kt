package tw.com.thinkPower.ThinkAPPrp

import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.child_fragment_connect.*
import kotlinx.android.synthetic.main.fragment_info.*
import org.json.JSONObject
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.Model.InfoModel
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException


class InfoFragment : Fragment() {

    private var socket = Socket()
    private var flag = 1
    private var mHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        if (ip.isNotEmpty()) {
            connectTask()
        }

    }

    private val reConnectToast = Runnable {
        if (infoIPstatus != null) {
            infoIPstatus.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (progressBar4 != null) {
            ip = ""
            infoIPstatus.setImageResource(R.drawable.ic_signal_gray)
            progressBar4.visibility = View.INVISIBLE
            Toast.makeText(context, "連線錯誤，請確認網路正常，重新設置ip！", Toast.LENGTH_SHORT).show()
        }
    }

    private fun connectTask () {
        if (socket.isConnected) {
            var connect = Connect()
            connect.sockect(socket)
            connect.closeIOAndSocket()
        }
        val t = Thread(readData)
        t.start()
    }

    private fun sendTask() {
        var t = Thread(sendCommand)
        t.start()
    }

    private val readData = Runnable {
        try {
            mHandler.post {
                if (progressBar4 != null) {
                    progressBar4.visibility = View.VISIBLE
                }
            }

            socket.connect(InetSocketAddress (ip, 1688),3000)

            if (socket.isConnected) {
                mHandler.post {
                    if (progressBar4 != null ) {
                        infoIPstatus.setImageResource(R.drawable.ic_signal_green)
                        progressBar4.visibility = View.INVISIBLE
                        sendTask()
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable{
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand("00000017FILEREAD,DeviceINFOPath")
        } catch (e:IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }

        if (msg != "" || !msg.contains("failed") || !msg.contains("ERROR")) {
            mHandler.post {
                if (progressBar4 != null) {
                    progressBar4.visibility = View.INVISIBLE
                    parse(msg)
                }
            }
        } else {
            mHandler.post {
                if (progressBar4 != null) {
                    AlertDialog.Builder(context)
                        .setMessage(msg)
                        .show()
                    progressBar4.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun parse(msg:String) {
        var version = context?.packageManager?.getPackageInfo(context?.packageName?:"", 0)?.versionName
        if(msg.contains("{")) {
            val response = msg.substring(msg.indexOf("{"))
            val listType = object : TypeToken<InfoModel>() {}.type
            val infoModel: InfoModel = Gson().fromJson(response, listType)
            infoTv1.text = infoModel.MachineModel
            infoTv2.text = infoModel.SeriesNumber
            infoTv3.text = infoModel.HWversion
            infoTv4.text = infoModel.FWversion
            infoTv5.text = infoModel.ManufactureDate
            infoTv6.text = infoModel.CalibrationDate
            infoTv7.text = "ThinkAPPrp_V$version"
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (socket.isConnected) {
            var c = Connect()
            c.sockect(socket)
            c.closeIOAndSocket()
        }
    }
}