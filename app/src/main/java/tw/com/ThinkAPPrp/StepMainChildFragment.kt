package tw.com.thinkPower.ThinkAPPrp

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.child_fragment_process_main.*
import kotlinx.android.synthetic.main.child_fragment_process_main.editText4
import kotlinx.android.synthetic.main.child_fragment_process_main.editText5
import kotlinx.android.synthetic.main.child_fragment_process_main.editText6
import kotlinx.android.synthetic.main.child_fragment_process_main.spinner2
import kotlinx.android.synthetic.main.child_fragment_process_main.spinner3
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.from
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processId
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList
import tw.com.thinkPower.ThinkAPPrp.Model.ProcessDataModel
import java.math.BigDecimal

class StepMainChildFragment : Fragment() {

    private var loopST1 = "0"
    private var loopST2 = "0"
    private var loopEND1 = "0"
    private var loopEND2 = "0"
    private var jump = "0"
    private var function = ""
    private var flag = intArrayOf(0,0,0)
    private var evcompset = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context)
            .inflate(R.layout.child_fragment_process_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    @SuppressLint("ResourceType")
    private fun initView() {

        if (from == "old" || from == "open") {
            getData()
            flag = intArrayOf(1,1,1)
        }

        editText2.addTextChangedListener(textWatcher(editText2,3))//current
        editText3.addTextChangedListener(textWatcher(editText3,3))//voltage
        editText4.addTextChangedListener(textWatcher(editText4,3))//power
        editText5.addTextChangedListener(textWatcher(editText5,1))//time
        editText6.addTextChangedListener(textWatcher(editText6,1))//record

        spinner1.onItemSelectedListener = SpinnerListener()

        spinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spinner1.selectedItemPosition == 0) {
                    if (flag[1] == 0) {
                        isEnabled(
                            flag[1],
                            true,
                            listOf(editText2, editText3, editText4, editText5, editText6)
                        )
                        when (position) {
                            0 -> {
                                val array = listOf("CC", "CC-CV")
                                val arrayAdapter = ArrayAdapter<String>(
                                    context!!,
                                    android.R.layout.simple_dropdown_item_1line,
                                    array
                                )
                                spinner3.isEnabled = true
                                spinner3.adapter = arrayAdapter
                            }
                            1 -> {
                                val array = listOf("CC", "CC-CV", "CC-CP")
                                val arrayAdapter = ArrayAdapter<String>(
                                    context!!,
                                    android.R.layout.simple_dropdown_item_1line,
                                    array
                                )
                                spinner3.isEnabled = true
                                spinner3.adapter = arrayAdapter
                            }
                            2 -> {
                                val array = listOf("")
                                val arrayAdapter = ArrayAdapter<String>(
                                    context!!,
                                    android.R.layout.simple_dropdown_item_1line,
                                    array
                                )
                                spinner3.adapter = arrayAdapter
                                spinner3.isEnabled = false
                            }
                        }
                    }
                }
                flag[1] = 0
            }
        }

        spinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spinner1.selectedItemPosition == 0) {
                    if (flag[2] == 0) {
                        isEnabled(flag[2],
                            true,
                            listOf(editText2, editText3, editText4, editText5, editText6)
                        )
                    }
                    when (position) {
                        0 -> {
                            //CC OR ""
                            when (spinner2.selectedItemPosition) {
                                0 -> {
                                    //CHARGE (evcompset == >= )
                                    evcompset = ">="
                                    isEnabled(0,false, listOf(editText3, editText4))
                                    if (flag[2] == 0) {
                                        editText2.setText("0")
                                        editText5.setText("10")
                                        editText6.setText("1")
                                    }
                                }
                                1 -> {
                                    //DISCHARGE (evcompset == <= )
                                    evcompset = "<="
                                    isEnabled(0,false, listOf(editText3, editText4))
                                    if (flag[2] == 0) {
                                        editText2.setText("0")
                                        editText5.setText("10")
                                        editText6.setText("1")
                                    }
                                }
                                2 -> {
                                    //REST (evcompset == 預設)
                                    evcompset = ">="
                                    isEnabled(0,false, listOf(editText2, editText3, editText4))
                                    if (flag[2] == 0) {
                                        editText5.setText("10")
                                        editText6.setText("1")
                                    }
                                }
                            }
                        }
                        1 -> {
                            //CC-CV
                            when (spinner2.selectedItemPosition) {
                                0 ->{
                                    //CHAREGE (evcompset == "" )
                                    evcompset = ""
                                    isEnabled(0,false, listOf(editText4))
                                    if (flag[2] == 0) {
                                        editText2.setText("0")
                                        editText3.setText("0")
                                        editText5.setText("10")
                                        editText6.setText("1")
                                    }
                                }
                                1 ->{
                                    //DISCHARGE (evcompset == "" )
                                    evcompset = ""
                                    isEnabled(0,false, listOf(editText4))
                                    if (flag[2] == 0) {
                                        editText2.setText("0")
                                        editText3.setText("0")
                                        editText5.setText("10")
                                        editText6.setText("1")
                                    }
                                }
                            }
                        }
                        2 -> {
                            //CC-CP (evcompset == <= )
                            evcompset = "<="
                            isEnabled(0,false, listOf(editText3))
                            if (flag[2] == 0) {
                                editText2.setText("0")
                                editText4.setText("0")
                                editText5.setText("10")
                                editText6.setText("1")
                            }
                        }
                    }
                }
                flag[2] = 0
            }
        }
    }

    private fun getData() {
        val function = resources.getStringArray(R.array.process_function)
        function.mapIndexed { index, c ->
            if (c.toString() == processList[processId].Function) {
                spinner1.setSelection(index)
            }
        }

        val action = resources.getStringArray(R.array.process_action)
        action.mapIndexed { index, c ->
            if (c.toString() == processList[processId].STEPAction) {
                spinner2.setSelection(index)
            }
        }

        var model = listOf<String>()
        when (processList[processId].STEPAction) {
            "CHARGE" -> model = listOf("CC","CC-CV")
            "DISCHARGE" -> model = listOf("CC","CC-CV","CC-CP")
            "REST" -> model = listOf("")
        }
        val modelAdapter = ArrayAdapter<String>(
            context!!,
            android.R.layout.simple_dropdown_item_1line,
            model
        )
        spinner3.adapter = modelAdapter
        model.mapIndexed { index, c ->
            if (c == processList[processId].STEPModel) {
                spinner3.setSelection(index)
            }
        }

        if (spinner2.selectedItem.toString() == "REST") {
            spinner3.isEnabled  = false
        }

        when (spinner1.selectedItem.toString()) {
            "LoopST1" -> {
                editText.setText(processList[processId].LoopST1)
            }
            "LoopST2" -> {
                editText.setText(processList[processId].LoopST2)
            }
            "Jump" -> {
                if (processList[processId].Jump?.contains("STEP") == true) {
                    //未找到step
                    var name = processList[processId].Jump?.replace("STEP","")?.toInt()?:0
                    editText.setText("$name")
                } else {
                    var id = processList[processId].Jump?.toInt()?: 0
                    processList.forEach {
                        if (it.id == id.toString()) {
                            editText.setText("${it.stepName?.replace("STEP","")?.toInt()?:0}")
                        }
                    }
                }
            }
        }

        editText2.setText(processList[processId].Iset)
        editText3.setText(processList[processId].Vset)
        editText4.setText(processList[processId].Pset)
        editText5.setText(processList[processId].Timeset)
        editText6.setText(processList[processId].RecordTimeset)

    }

    private fun saveData() {

        when (spinner1.selectedItem.toString()) {
            "NA" -> {
                function = "NA"
                loopST1 = "0"
                loopST2 = "0"
                jump = "0"
                loopEND1 = "0"
                loopEND2 = "0"
            }
            "LoopST1" -> {
                function = "LoopST1"
                loopST1 = editText.text.toString()
                loopST2 = "0"
                jump = "0"
                loopEND1 = "0"
                loopEND2 = "0"
            }
            "LoopST2" -> {
                function = "LoopST2"
                loopST1 = "0"
                loopST2 = editText.text.toString()
                jump = "0"
                loopEND1 = "0"
                loopEND2 = "0"
        }
            "LoopEND1" -> {
                function = "LoopEND1"
                loopST1 = "0"
                loopST2 = "0"
                jump = "0"
                loopEND2 = "0"
                // loopEnd1 save on spinner listener
            }
            "LoopEND2" -> {
                function = "LoopEND2"
                loopST1 = "0"
                loopST2 = "0"
                jump = "0"
                loopEND1 = "0"
                // loopEnd2 save on spinner listener
            }
            "Jump" -> {
                function = "Jump"
                loopST1 = "0"
                loopST2 = "0"
                jump = changeToId(editText.text.toString())
                loopEND1 = "0"
                loopEND2 = "0"
            }
            "END" -> {
                function = "END"
                loopST1 = "0"
                loopST2 = "0"
                jump = "0"
                loopEND1 = "0"
                loopEND2 = "0"
            }
        }

        if (from == "new") {
            //new step
            var endPos:Int? = null
            var endId:String? = null
            processList.mapIndexed { index, processModel ->
                if (processModel.Function == "END") {
                    endId = processModel.id
                    endPos = index
                }
            }

            if(endPos != null && endId != null) {
                var name = convertName((endPos!!+1).toString())
                checkValue(name)
                var processModel = ProcessDataModel(
                    "","",processId.toString(), name,function,loopST1,loopST2,loopEND1,loopEND2,jump, spinner2.selectedItem.toString(), spinner3.selectedItem.toString(), editText3.text.toString(), editText2.text.toString(),editText4.text.toString(), editText5.text.toString(), editText6.text.toString(), "0", "0", "0", "0", "0", evcompset, "0", "0", "0", "0", "0", "0",">=","0","")
                //將new step 取代為 end step位置
                processList[endPos!!] = processModel
                processList.add(ProcessDataModel("","",endId?:"",convertName((processList.size+1).toString()),"END","0","0","0","0","0","","","0","0","0","0","0","0","0","0","0","0","","0","0","0","0","0","0","","0",""))
            }

            from = "old"
            for (i in processList.indices) {
                if (processList[i].id == processId.toString()) {
                    //改存position
                    processId = i
                    break
                }
                Log.i("proceeList$i", processList[i].id+processList[i].stepName)
            }

        } else {
            //edit step
            processList[processId].Function = function
            processList[processId].LoopST1 = loopST1
            processList[processId].LoopST2 = loopST2
            processList[processId].LoopEND1 = loopEND1
            processList[processId].LoopEND2 = loopEND2
            processList[processId].Jump = jump
            processList[processId].STEPAction = spinner2?.selectedItem.toString()
            processList[processId].STEPModel = spinner3?.selectedItem.toString()
            processList[processId].Vset = editText3?.text.toString()
            processList[processId].Iset = editText2?.text.toString()
            processList[processId].Pset = editText4?.text.toString()
            processList[processId].Timeset = editText5?.text.toString()
            processList[processId].RecordTimeset = editText6?.text.toString()
        }
    }

    private fun convertName (name:String):String {
        var len = name.length
        var zero = ""
        if (len < 4) {
            for (i in len until 4) {
                zero += "0"
            }
        }
        return "STEP$zero${name}"
    }

    private fun checkValue(name: String){
        //檢查其他step有無Goto是到此step的，有則改為此step id

        processList.forEach {
            if (it.Function == "Jump" && it.Jump == name) {
                it.Jump = processId.toString()
            }
//            if (it.Function == "LoopEND1" && it.LoopEND1 == name) {
//                it.LoopEND1 = processId.toString()
//            }
//            if (it.Function == "LoopEND2" && it.LoopEND2 == name) {
//                it.LoopEND2 = processId.toString()
//            }
            if (it.mAhCdtset?.contains(name) == true) {
                it.mAhCdtset = processId.toString()
            }
            if (it.WhCdtset?.contains(name) == true) {
                it.WhCdtset = processId.toString()
            }
            if (it.EVCdtset?.contains(name) == true) {
                it.EVCdtset = processId.toString()
            }
            if (it.ECCdtset?.contains(name) == true) {
                it.ECCdtset = processId.toString()
            }
            if (it.mdVCdtset?.contains(name) == true) {
                it.mdVCdtset = processId.toString()
            }
            if (it.ETCdtset?.contains(name) == true) {
                it.ETCdtset = processId.toString()
            }
        }
    }

    private fun changeToId (stepName:String):String {
        //檢查GOTO要去的STEP是否已存在，有則將GOTO的VALUE改成該STEP的ID，無則存STEP NAME
        var len = stepName.length
        var zero = ""
        if (len < 4) {
            for (i in len until 4) {
                zero += "0"
            }
        }
        var id = "STEP$zero$stepName"
        processList.forEach {
            if (it.stepName == id) {
                id = it.id?:""
            }
        }
        return id
    }

    private fun isEnabled(flag:Int,boolean: Boolean,etList:List<EditText>) {
        for (i in etList.indices) {
            if (flag == 0) {
                if (!boolean) {
                    etList[i].setText("0")
                } else {
                    etList[i].setText("")
                }
            }
            etList[i].isEnabled = boolean
//            etList[i].isFocusableInTouchMode = boolean
        }
    }

    inner class SpinnerListener : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(
            parent: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ) {
            if (flag[0] == 0) {
                isEnabled(flag[0],
                    true,
                    listOf(editText, editText2, editText3, editText4, editText5, editText6)
                )
            }
            when (position) {
                0 -> {
                    if (flag[0] == 0) {
                        spinner2.isEnabled = true
                        spinner3.isEnabled = true
                        editText.isEnabled = false
                        val actionArray = context?.resources?.getStringArray(R.array.process_action)
                        val actionAdapter = ArrayAdapter<String>(
                            context!!,
                            android.R.layout.simple_dropdown_item_1line,
                            actionArray!!
                        )
                        spinner2.adapter = actionAdapter
                        val array = listOf("CC", "CC-CV", "CC-CP")
                        val arrayAdapter = ArrayAdapter<String>(
                            context!!,
                            android.R.layout.simple_dropdown_item_1line,
                            array
                        )
                        spinner3.adapter = arrayAdapter
                        editText.setText("")
                    }
                }
                1 -> {
                    //loopst1
                    spinner2.isEnabled = false
                    spinner3.isEnabled = false
                    editText.isEnabled = true
                    if (flag[0] == 0 ) {
                        editText.setText("0")
                    }
                    val array = listOf("")
                    val arrayAdapter = ArrayAdapter<String>(
                        context!!,
                        android.R.layout.simple_dropdown_item_1line,
                        array
                    )
                    spinner2.adapter = arrayAdapter
                    spinner3.adapter = arrayAdapter
                    isEnabled(flag[0],
                        false,
                        listOf(editText2, editText3, editText4, editText5, editText6)
                    )

                    for (i in processList.indices) {
                        if (processList[i].Function == "LoopEND1") {
                            processList[i].LoopEND1 = convertName((processList.size+1).toString())
                        }
                    }

                }
                2 -> {
                    //loopend1
                    spinner2.isEnabled = false
                    spinner3.isEnabled = false
                    editText.isEnabled = false
                    val array = listOf("")
                    val arrayAdapter = ArrayAdapter<String>(
                        context!!,
                        android.R.layout.simple_dropdown_item_1line,
                        array
                    )
                    spinner2.adapter = arrayAdapter
                    spinner3.adapter = arrayAdapter

                    for (i in processList.indices) {
                        if (processList[i].Function == "LoopST1") {
                            //不用存id，只需要找st1下一個step
                            loopEND1 = processList[i+1].stepName?.replace("STEP","")?.toInt().toString()
                            editText.setText(
                                "${processList[i + 1].stepName?.replace(
                                    "STEP",
                                    ""
                                )?.toInt() ?: 0}"
                            )
                            break
                        } else {
                            //if loopST1 not found back to step0001
                            loopEND1 = "1"
                            editText.setText("1")
                        }
                    }

                    isEnabled(flag[0],
                        false,
                        listOf(editText2, editText3, editText4, editText5, editText6)
                    )
                }
                3 -> {
                    //loopst2
                    spinner2.isEnabled = false
                    spinner3.isEnabled = false
                    editText.isEnabled = true
                    if (flag[0] == 0 ) {
                        editText.setText("0")
                    }
                    val array = listOf("")
                    val arrayAdapter = ArrayAdapter<String>(
                        context!!,
                        android.R.layout.simple_dropdown_item_1line,
                        array
                    )
                    spinner2.adapter = arrayAdapter
                    spinner3.adapter = arrayAdapter
                    isEnabled(flag[0],
                        false,
                        listOf(editText2, editText3, editText4, editText5, editText6)
                    )

                    for (i in processList.indices) {
                        if (processList[i].Function == "LoopEND2") {
                            processList[i].LoopEND2 = convertName((processList.size+1).toString())
                        }
                    }

                }
                4 -> {
                    //loopend2
                    spinner2.isEnabled = false
                    spinner3.isEnabled = false
                    editText.isEnabled = false
                    val array = listOf("")
                    val arrayAdapter = ArrayAdapter<String>(
                        context!!,
                        android.R.layout.simple_dropdown_item_1line,
                        array
                    )
                    spinner2.adapter = arrayAdapter
                    spinner3.adapter = arrayAdapter

                    for (i in processList.indices) {
                        if (processList[i].Function == "LoopST2") {
                            //不用存id，只需要找st1下一個step
                            loopEND2 = processList[i+1].stepName?.replace("STEP","")?.toInt().toString()
                            editText.setText(
                                "${processList[i + 1].stepName?.replace(
                                    "STEP",
                                    ""
                                )?.toInt() ?: 0}"
                            )
                            break
                        } else {
                            //if loopST1 not found back to step0001
                            loopEND2 = "1"
                            editText.setText("1")
                        }
                    }

                    isEnabled(flag[0],
                        false,
                        listOf(editText2, editText3, editText4, editText5, editText6)
                    )

                }
                5 -> {
                    //jump
                    spinner2.isEnabled = false
                    spinner3.isEnabled = false
                    editText.isEnabled = true
                    val array = listOf("")
                    val arrayAdapter = ArrayAdapter<String>(
                        context!!,
                        android.R.layout.simple_dropdown_item_1line,
                        array
                    )
                    spinner2.adapter = arrayAdapter
                    spinner3.adapter = arrayAdapter
                    isEnabled(flag[0],
                        false,
                        listOf(editText2, editText3, editText4, editText5, editText6)
                    )
                }
//                6 -> {
//                    //end
//                    spinner2.isEnabled = false
//                    spinner3.isEnabled = false
//                    editText.isEnabled = true
//                    editText.isFocusableInTouchMode = true
//                    val array = listOf("")
//                    val arrayAdapter = ArrayAdapter<String>(
//                        context!!,
//                        android.R.layout.simple_dropdown_item_1line,
//                        array
//                    )
//                    spinner2.adapter = arrayAdapter
//                    spinner3.adapter = arrayAdapter
//                    isEnabled(flag[0],
//                        false,
//                        listOf(editText, editText2, editText3, editText4, editText5, editText6)
//                    )
//                }
            }
            flag[0] = 0
        }
    }

    inner class textWatcher(val editText: EditText,val limit:Int): TextWatcher{
        override fun afterTextChanged(s: Editable?) {
            if (s?.indexOf(".") != -1 ) {
                if (s?.substring(s.indexOf(".")+1)?.length?:0 > limit) {
                    editText.setText(s?.substring(0,s.indexOf(".")+limit+1))
                    editText.setSelection(editText.length())
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) {
            saveData()
            Log.d("main","hide")
        }
    }

    override fun onPause() {
        super.onPause()
        saveData()
    }

}