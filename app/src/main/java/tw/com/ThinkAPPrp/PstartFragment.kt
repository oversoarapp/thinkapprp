package tw.com.thinkPower.ThinkAPPrp

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.implments.SwipeItemMangerImpl
import kotlinx.android.synthetic.main.fragment_pstart.*
import tw.com.thinkPower.ThinkAPPrp.Adapter.PstartAdapter
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException

class PstartFragment : Fragment() {

    private var flag = 1
    private var socket = Socket()
    private var command = "0000000FProcessFILEREAD"
    private var mHandler:Handler? = null
    private var nAdapter: PstartAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_pstart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        mHandler = Handler()
        
        if (ip.isNotEmpty()) {
            connectTask()
        }

        val click:(String) -> Unit = {
            var tmp = "START,$it"
            var zero = ""
            var hex = Integer.toHexString(tmp.length)
            var size = 8 - hex.length
            for (i in 0 until size) {
                zero += "0"
            }
            command = zero + hex + tmp
            pstartButton.visibility = View.VISIBLE
        }

        val deleteClick:(String, Int, SwipeLayout, SwipeItemMangerImpl) -> Unit = { name, pos, layout, manger ->
        }

        nAdapter = PstartAdapter(
            context!!,
            false,
            click,
            deleteClick
        )

        pstartRv.layoutManager = LinearLayoutManager(context)
        pstartRv.adapter = nAdapter

        pstartButton.setOnClickListener {
            if (command != "") {
                connectTask()
            }
        }

        pstartRefresh.setOnClickListener {
            if (ip.isNotEmpty()) {
                command = "0000000FProcessFILEREAD"
                connectTask()
            }
        }

    }

    private val reConnectToast = Runnable {
        if (pstartIPstatus != null ) {
            pstartIPstatus.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (progressBar2 != null && pstartIPstatus != null) {
            progressBar2.visibility = View.INVISIBLE
            ip = ""
            pstartIPstatus.setImageResource(R.drawable.ic_signal_gray)
            Toast.makeText(context, "連線錯誤，請確認網路正常，重新設置ip！", Toast.LENGTH_SHORT).show()
        }
    }

    private fun connectTask () {
        val t = Thread(readData)
        t.start()
    }

    private fun sendTask() {
        var t = Thread(sendCommand)
        t.start()
    }

    private val readData = Runnable {
        try {
            mHandler?.post{
                if (progressBar2 != null) {
                    progressBar2.visibility = View.VISIBLE
                }
            }
            socket = Socket()
            socket.connect(InetSocketAddress (ip, 1688),3000)

            if (socket.isConnected) {
                mHandler?.post {
                    if (pstartIPstatus != null) {
                        pstartIPstatus.setImageResource(R.drawable.ic_signal_green)
                        progressBar2.visibility = View.INVISIBLE
                        sendTask()
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable{
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand(command)
        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }

        if (msg.isEmpty() || msg.contains("00000034The current operating state does not allow starting.") || msg.contains("failed") || msg.contains("error")) {
            when(msg) {
                "00000034The current operating state does not allow starting." -> msg = msg.substring(8)
                "" -> msg = "something wrong,please try again"
            }
            mHandler?.post {
                if (progressBar2 != null) {
                    progressBar2.visibility = View.INVISIBLE
                    AlertDialog.Builder(context)
                        .setMessage(msg)
                        .show()
                }
            }
        } else {
            if (command.contains("ProcessFILEREAD")) {
                //Process List
                var list = msg.substring(8).replace(".json", "").split(",").toMutableList()
                mHandler?.post {
                    if (progressBar2 != null) {
                        progressBar2.visibility = View.INVISIBLE
                        nAdapter?.updateData(list.sorted().toMutableList())
                    }
                }
            } else {
                if (command.contains("START")) {
                    //PSTART RETURN OK
                    mHandler?.post {
                        if (progressBar2 != null) {
                            progressBar2.visibility = View.INVISIBLE
                            Toast.makeText(context, "OK", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mHandler = null
        if (progressBar2 != null && progressBar2.visibility == View.VISIBLE) {
            progressBar2.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        var c = Connect()
        c.sockect(socket)
        c.closeIOAndSocket()
    }
}