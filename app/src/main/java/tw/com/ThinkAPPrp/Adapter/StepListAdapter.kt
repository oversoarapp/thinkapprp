import android.content.Context
import android.graphics.Color
import com.daimajia.swipe.SwipeLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList
import tw.com.thinkPower.ThinkAPPrp.Model.ProcessDataModel
import tw.com.thinkPower.ThinkAPPrp.R
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import StepListAdapter.SimpleViewHolder
import android.widget.LinearLayout
import java.util.*

class StepListAdapter (context:Context,dataList: MutableList<ProcessDataModel>, val click: (Int) -> Unit, val choose:(String)->Unit) : RecyclerSwipeAdapter<StepListAdapter.SimpleViewHolder>() {

    private var context = context
    private var dataList = dataList
    private var tmp:String? = null
    private var lastClickTime = 0L

    fun updateData(dataList:MutableList<ProcessDataModel>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val swipeLayout: SwipeLayout = itemView.findViewById(R.id.swipe)
        val valueLayout: LinearLayout = itemView.findViewById(R.id.linearLayout)
        val bottom_wrapper: LinearLayout = itemView.findViewById(R.id.bottom_wrapper)
        val bottom_wrapper2: LinearLayout = itemView.findViewById(R.id.bottom_wrapper2)
        val textView3:TextView = itemView.findViewById(R.id.text3)
        val textView2:TextView = itemView.findViewById(R.id.text2)
        val textView1:TextView = itemView.findViewById(R.id.text1)
        val buttonDelete: Button = itemView.findViewById(R.id.delete)
        val buttonCopyNext: Button = itemView.findViewById(R.id.copyNext)
        val buttonCopyLast: Button = itemView.findViewById(R.id.copyLast)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_processlist, parent, false)
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: SimpleViewHolder, position: Int) {
        val item = dataList[position]
        viewHolder.swipeLayout.showMode = SwipeLayout.ShowMode.PullOut

        if (item.Function != "NA") {
            viewHolder.textView2.text = item.Function
        } else {
            viewHolder.textView2.text = item.STEPAction
        }

        if (item.STEPModel == "" ) {
            viewHolder.textView3.text = ""
        } else {
            viewHolder.textView3.text = item.STEPModel
        }

        viewHolder.textView1.text = dataList.getOrNull(position)?.stepName
        viewHolder.buttonCopyNext.setOnClickListener {
            if (item.Function != "END") {
                var zero = ""
                var name = dataList[position].stepName?.replace("STEP", "")?.toInt() ?: 0
                var size = 4 - (name + 1).toString().length
                for (i in 0 until size) {
                    zero += "0"
                }
                var id = (dataList.size+1).toString()
                for (i in dataList.indices) {
                    if (dataList[i].id == (dataList.size+1).toString()) {
                        id = (dataList.size+2).toString()
                    }
                }
                var step = dataList[position].copy(
                    stepName = "STEP$zero${name + 1}",
                    id = id
                )
                var tmpList = mutableListOf<ProcessDataModel>()
                tmpList.add(step)
                for (i in position + 1 until dataList.size) {
                    var zero = ""
                    var size = 4 - (i + 2).toString().length
                    for (i in 0 until size) {
                        zero += "0"
                    }
                    dataList[i].stepName = "STEP$zero${i + 2}"
                    tmpList.add(dataList[i])
                }
                dataList.add(step)
                for (i in position + 1 until dataList.size) {
                    dataList[i] = tmpList[0]
                    tmpList.removeAt(0)
                }
                processList = dataList
                notifyDataSetChanged()
                mItemManger.removeShownLayouts(viewHolder.swipeLayout)
                mItemManger.closeItem(position)
            }
        }

        viewHolder.buttonCopyLast.setOnClickListener {
            if (item.Function != "END") {
                var name: String? = null
                var endPos: Int? = null
                dataList.mapIndexed { index, processDataModel ->
                    if (processDataModel.Function == "END") {
                        name = processDataModel.stepName
                        endPos = index
                    }
                }
                if (endPos != null) {
                    var id = (dataList.size+1).toString()
                    for (i in dataList.indices) {
                        if (dataList[i].id == (dataList.size+1).toString()) {
                            id = (dataList.size+2).toString()
                        }
                    }
                    var zero = ""
                    var size = 4 - (endPos!! + 1).toString().length
                    for (i in 0 until size) {
                        zero += "0"
                    }
                    var step = dataList[position].copy(stepName = name, id = id)
                    var end = dataList[endPos!!].copy(stepName = "STEP$zero${endPos!! + 2}")
                    dataList[endPos ?: 0] = step
                    dataList.add(end)
                }
                processList = dataList
                notifyDataSetChanged()
                mItemManger.removeShownLayouts(viewHolder.swipeLayout)
                mItemManger.closeAllItems()
            }
        }

        viewHolder.buttonDelete.setOnClickListener { view ->
            if (item.Function != "END") {
                for (i in position + 1 until dataList.size) {
                    var name = (dataList[i].stepName?.replace("STEP", "")?.toInt() ?: 0) - 1
                    var zero = ""
                    for (i in 0 until 4 - (name.toString().length)) {
                        zero += "0"
                    }
                    dataList[i].stepName = "STEP$zero$name"
                }
                mItemManger.removeShownLayouts(viewHolder.swipeLayout)
                dataList.removeAt(position)
                notifyItemRemoved(position)
                mItemManger.closeAllItems()
                notifyItemRangeChanged(position, dataList.size)
                processList = dataList
            }
        }

        viewHolder.valueLayout.setOnClickListener {
            if (dataList[position].Function != "END") {
                val now = Date().time
                if (now - lastClickTime <= 180) {
                    click(position)
                } else {
                    choose(item.id ?: "")
                    tmp = item.id
                    notifyDataSetChanged()
                }
                lastClickTime = now
            }
        }

        if (tmp == item.id) {
            viewHolder.itemView.background = context.getDrawable(R.color.colorGrey)
        } else {
            viewHolder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }

        mItemManger.bindView(viewHolder.itemView, position)

    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

}