package tw.com.thinkPower.ThinkAPPrp.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import tw.com.thinkPower.ThinkAPPrp.R

class ShowIpAdapter(context: Context, val click:(String)->Unit, val delete:(Int)->Unit ): RecyclerSwipeAdapter<ShowIpAdapter.ViewHolder>() {

    var dataList: MutableList<String>? =null
    var tmp:Int? = null
    var context = context

    fun updateData(dataList:MutableList<String>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.swipeLayout.showMode = SwipeLayout.ShowMode.PullOut
        holder.ip.text = dataList?.getOrNull(position)

        holder.valueLayout.setOnClickListener {
            click(dataList?.get(position)?:"")
            tmp = position
            notifyDataSetChanged()
        }

        holder.delete.setOnClickListener {
            delete(position)
            dataList?.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, dataList?.size?:0)
            mItemManger.closeAllItems()
            mItemManger.removeShownLayouts(holder.swipeLayout)
        }

        if (tmp == position) {
            holder.itemView.background = context.getDrawable(R.color.colorGrey)
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }

        mItemManger.bindView(holder.itemView, position)

    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val ip: TextView = itemView.findViewById(R.id.itemListTv)
        val swipeLayout: SwipeLayout = itemView.findViewById(R.id.swipe)
        val delete = itemView.findViewById<Button>(R.id.delete)
        val valueLayout = itemView.findViewById<LinearLayout>(R.id.linearLayout)
    }

}