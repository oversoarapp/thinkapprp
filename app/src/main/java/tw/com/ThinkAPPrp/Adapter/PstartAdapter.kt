package tw.com.thinkPower.ThinkAPPrp.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.daimajia.swipe.implments.SwipeItemMangerImpl
import tw.com.thinkPower.ThinkAPPrp.R
import java.util.*

class PstartAdapter(context: Context,isEditOpenFragment:Boolean, val click: (String) -> Unit, val deleteClick: (String, Int, SwipeLayout, SwipeItemMangerImpl) -> Unit): RecyclerSwipeAdapter<PstartAdapter.ViewHolder>() {

    private var context = context
    private var isEditOpenFragment = isEditOpenFragment
    var dataList:MutableList<String>? = null
    var tmp:Int? = null
    private var lastClickTime = 0L

    fun updateData(dataList:MutableList<String>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position:Int ) {

        if (isEditOpenFragment) {
            holder.deleteLayout.visibility = View.VISIBLE
            holder.swipeLayout.showMode = SwipeLayout.ShowMode.PullOut

            holder.valueLayout.setOnClickListener {
                val now = Date().time
                if (now - lastClickTime <= 180) {
                    tmp = position
                    click(dataList?.getOrNull(position) ?: "")
                    notifyDataSetChanged()
                } else {
                    tmp = position
                    notifyDataSetChanged()
                }
                lastClickTime = now
            }

        } else {
            holder.deleteLayout.visibility = View.GONE
            holder.valueLayout.setOnClickListener {
                tmp = position
                click(dataList?.getOrNull(position) ?:"")
                notifyDataSetChanged()
            }
        }

        holder.delete.setOnClickListener {
            deleteClick(dataList?.getOrNull(position)?:"",position,holder.swipeLayout,mItemManger)
        }

        holder.itemListTv.text = dataList?.getOrNull(position)?:""

        if (tmp == position) {
            holder.itemView.background = context.getDrawable(R.color.colorGrey)
        } else {
            holder.itemView.setBackgroundColor(Color.TRANSPARENT)
        }

        mItemManger.bindView(holder.itemView, position)

    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        val itemListTv = itemView.findViewById<TextView>(R.id.itemListTv)
        val delete = itemView.findViewById<Button>(R.id.delete)
        val swipeLayout = itemView.findViewById<SwipeLayout>(R.id.swipe)
        val deleteLayout = itemView.findViewById<LinearLayout>(R.id.bottom_wrapper)
        val valueLayout = itemView.findViewById<LinearLayout>(R.id.linearLayout)
    }

}