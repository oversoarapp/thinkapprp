package tw.com.thinkPower.ThinkAPPrp

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.child_fragment_process_condition.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList
import tw.com.thinkPower.ThinkAPPrp.Model.ProcessDataModel
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import kotlinx.android.synthetic.main.child_fragment_process_condition.spinner2
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.from
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processId
import java.math.BigDecimal


class StepCondiChildFragment: Fragment() {

    private var spinnerArray = listOf<String>()
    private var mHandler = Handler()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.child_fragment_process_condition, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        et1.addTextChangedListener(textWatcher(et1,3))//ev
        et3.addTextChangedListener(textWatcher(et3,3))//ec
        et5.addTextChangedListener(textWatcher(et5,0))//mah
        et7.addTextChangedListener(textWatcher(et7,3))//wh
        et9.addTextChangedListener(textWatcher(et9,3))//dv
        et11.addTextChangedListener(textWatcher(et11,1))//endTemperature

        spinnerArray = listOf(">=","<=")
        val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, spinnerArray)
        spinner.adapter = arrayAdapter
        spinner8.adapter = arrayAdapter

        var list = listOf(et1, et2, et3, et4, et5, et6, et7, et8, et9, et10, et11, et12)
        isEnabled(true, list)
        var sList = listOf(
            spinner,
            spinner2,
            spinner4,
            spinner5,
            spinner6,
            spinner7,
            spinner8,
            spinner9
        )
        sList.forEach {
            it.isEnabled = true
        }

        var mList = listOf(spinner2, spinner4, spinner5, spinner7, spinner6, spinner9)
        var etList = listOf(et2, et4, et6, et8, et10, et12)
        for (i in mList.indices) {
            mList[i].onItemSelectedListener = GotoItemSelected(etList[i])
        }

        getData()

    }

    private fun getData() {

        spinnerArray.mapIndexed { index, s ->
            if (s == processList[processId].EVCompset) {
                spinner.setSelection(index)
            }
            if (s == processList[processId].ETCompset) {
                spinner8.setSelection(index)
            }
        }

        var list = listOf(processList[processId].EVset?:"",processList[processId].ECset?:"",processList[processId].mAhset?:"",processList[processId].Whset?:"",processList[processId].mdVset?:"",processList[processId].ETset?:"")
        var vlist = listOf(processList[processId].EVCdtset?:"",processList[processId].ECCdtset?:"",processList[processId].mAhCdtset?:"",processList[processId].WhCdtset?:"",processList[processId].mdVCdtset?:"",processList[processId].ETCdtset?:"")
        var sList = listOf(spinner2,spinner4,spinner5,spinner7,spinner6,spinner9)
        var eList = listOf(et2,et4,et6,et8,et10,et12)
        for (i in list.indices) {
            setData(list[i],vlist[i],sList[i],eList[i])
        }

        var valueEtList = listOf(et1,et3,et5,et7,et9,et11)
        for (i in list.indices){
            if (list[i] != "") {
                valueEtList[i].setText(list[i])
            }
        }
        restrict()
    }

    private fun saveData() {
        processList[processId].EVset = et1.text.toString()
        processList[processId].EVCdtset = changeToId(spinner2,et1.text.toString(),et2.text.toString())
        processList[processId].EVCompset = spinner.selectedItem.toString()
        processList[processId].ECset = et3.text.toString()
        processList[processId].ECCdtset = changeToId(spinner4,et3.text.toString(),et4.text.toString())
        processList[processId].mAhset = et5.text.toString().toInt().toString()
        processList[processId].mAhCdtset = changeToId(spinner5,et5.text.toString().toDouble().toInt().toString(),et6.text.toString())
        processList[processId].Whset = et7.text.toString()
        processList[processId].WhCdtset = changeToId(spinner7,et7.text.toString(),et8.text.toString())
        processList[processId].mdVset = et9.text.toString()
        processList[processId].mdVCdtset = changeToId(spinner6,et9.text.toString(),et10.text.toString())
        processList[processId].ETset = et11.text.toString()
        processList[processId].ETCdtset = changeToId(spinner9,et11.text.toString(),et12.text.toString())
        processList[processId].ETCompset = spinner8.selectedItem.toString()
    }

    @SuppressLint("SetTextI18n")
    private fun setData(mValue:String, value:String, spinner: Spinner, et:EditText) {
        //此value為step id,如value = next = step
        var valueStep: ProcessDataModel? = null
        if (mValue != "0.0" && mValue != "0" && mValue.isNotEmpty()) {
            if (value == "" || value == "0" || value.contains("Next") || value.contains("STEP")) {
                when (value) {
                    "0" -> {
                        spinner.setSelection(0)
                        et.isEnabled = false
                        et.isFocusableInTouchMode = false
                        et.setText("")
                    }
                    "" -> {
                        spinner.setSelection(0)
                        et.isEnabled = false
                        et.isFocusableInTouchMode = false
                        et.setText("")
                    }
                    else -> {
                        if (value.contains("Next")) {
                            spinner.setSelection(0)
                            et.isEnabled = false
                            et.isFocusableInTouchMode = false
                            et.setText("")
                        }
                        if (value.contains("STEP")) {
                            //該step尚未建立
                            spinner.setSelection(1)
                            et.isEnabled = false
                            et.isFocusableInTouchMode = false
                            et.setText("${value.replace("STEP","").toInt()}")
                        }
                    }
                }
            } else {

                processList.forEach {
                    if (value == it.id) {
                        valueStep = it
                    }
                }

                if (valueStep != null) {
                    var name = valueStep!!.stepName?.replace("STEP", "")?.toInt() ?: 0
                    when {
                        valueStep!!.Function == "END" -> {
                            //end
                            spinner.setSelection(2)
                            et.setText("")
                            et.isEnabled = false
                            et.isFocusableInTouchMode = false
                        }
                        processList[processId + 1] == valueStep -> {
                            //next
                            spinner.setSelection(0)
                            et.setText("")
                            et.isEnabled = false
                            et.isFocusableInTouchMode = false
                        }
                        else -> {
//                        //step
                            spinner.setSelection(1)
                            et.isEnabled = true
                            et.isFocusableInTouchMode = true
                            et.setText("$name")
                        }
                    }
                }
            }
        } else {
            spinner.setSelection(0)
            et.setText("")
        }
    }

    private fun restrict() {
        if (processList[processId].Function == "LoopST1") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner8.adapter = arrayAdapter
            var sList = listOf(spinner,spinner2,spinner4,spinner5,spinner6,spinner7,spinner8,spinner9)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et1,et2,et3,et4,et5,et6,et7,et8,et9,et10,et11,et12)
            isEnabled(false,list)
        }
        if (processList[processId].Function == "LoopST2") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner8.adapter = arrayAdapter
            var sList = listOf(spinner,spinner2,spinner4,spinner5,spinner6,spinner7,spinner8,spinner9)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et1,et2,et3,et4,et5,et6,et7,et8,et9,et10,et11,et12)
            isEnabled(false,list)
        }
        if (processList[processId].Function == "LoopEND1") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner8.adapter = arrayAdapter
            var sList = listOf(spinner,spinner2,spinner4,spinner5,spinner6,spinner7,spinner8,spinner9)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et1,et2,et3,et4,et5,et6,et7,et8,et9,et10,et11,et12)
            isEnabled(false,list)
        }
        if (processList[processId].Function == "LoopEND2") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner8.adapter = arrayAdapter
            var sList = listOf(spinner,spinner2,spinner4,spinner5,spinner6,spinner7,spinner8,spinner9)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et1,et2,et3,et4,et5,et6,et7,et8,et9,et10,et11,et12)
            isEnabled(false,list)
        }
        if (processList[processId].Function == "Jump") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner8.adapter = arrayAdapter
            var sList = listOf(spinner,spinner2,spinner4,spinner5,spinner6,spinner7,spinner8,spinner9)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et1,et2,et3,et4,et5,et6,et7,et8,et9,et10,et11,et12)
            isEnabled(false,list)
        }
        if (processList[processId].Function == "END") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner8.adapter = arrayAdapter
            var sList = listOf(spinner,spinner2,spinner4,spinner5,spinner6,spinner7,spinner8,spinner9)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et1,et2,et3,et4,et5,et6,et7,et8,et9,et10,et11,et12)
            isEnabled(false,list)
        }
        if (processList[processId].Function == "NA" && processList[processId].STEPAction == "CHARGE" && processList[processId].STEPModel == "CC") {
            var sList = listOf(spinner4)
            sList.forEach {
                spinnerSetting(it)
            }
            isEnabled(false, listOf(et3,et4))
            spinner.isEnabled = false
        }
        if (processList[processId].Function == "NA" && processList[processId].STEPAction == "CHARGE" && processList[processId].STEPModel == "CC-CV") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner.isEnabled = false
            var sList = listOf(spinner2,spinner6)
            sList.forEach {
                spinnerSetting(it)
            }
            isEnabled(false, listOf(et1,et2,et9,et10))
        }
        if (processList[processId].Function == "NA" && processList[processId].STEPAction == "DISCHARGE" && processList[processId].STEPModel == "CC") {
            var sList = listOf(spinner4,spinner6)
            sList.forEach {
                spinnerSetting(it)
            }
            isEnabled(false, listOf(et3,et4,et9,et10))
//            spinner.setSelection(1)
            spinner.isEnabled = false
        }
        if (processList[processId].Function == "NA" && processList[processId].STEPAction == "DISCHARGE" && processList[processId].STEPModel == "CC-CV") {
            val compset = listOf("")
            val arrayAdapter = ArrayAdapter<String>(context!!, android.R.layout.simple_dropdown_item_1line, compset)
            spinner.adapter = arrayAdapter
            spinner.isEnabled = false
            var sList = listOf(spinner2,spinner6)
            sList.forEach {
                spinnerSetting(it)
            }
            isEnabled(false, listOf(et1,et2,et9,et10))
        }
        if (processList[processId].Function == "NA" && processList[processId].STEPAction == "DISCHARGE" && processList[processId].STEPModel == "CC-CP") {
            var sList = listOf(spinner4,spinner6)
            sList.forEach {
                spinnerSetting(it)
            }
            isEnabled(false, listOf(et3,et4,et9,et10))
            spinner.isEnabled = false
        }
        if (processList[processId].Function == "NA" && processList[processId].STEPAction == "REST") {
            var sList = listOf(spinner4,spinner5,spinner6,spinner7)
            sList.forEach {
                spinnerSetting(it)
            }
            var list = listOf(et3,et4,et5,et6,et7,et8,et9,et10)
            isEnabled(false, list)
        }
        var sList = listOf(spinner2, spinner4, spinner5, spinner7, spinner6, spinner9)
        var etList = listOf(et2, et4, et6, et8, et10, et12)
        check(sList,etList)
    }

    private fun isEnabled(boolean: Boolean,etList:List<EditText>) {
        for (i in etList.indices) {
            etList[i].setText("0")
            etList[i].isEnabled = boolean
            etList[i].isFocusableInTouchMode = boolean
        }
    }

    private fun spinnerSetting (spinner: Spinner) {
        spinner.isEnabled = false
        spinner.setSelection(0)
        spinner.onItemSelectedListener = null
    }

    private fun changeToId (spinner: Spinner,value:String,stepName:String):String {
        //如果主參數為0，goto設定的參數無效
        if (value == "0" || value == "0.0" || value.isEmpty() ) {
            return "0"
        } else {
            //檢查GOTO要去的STEP是否已存在，有則將GOTO的VALUE改成該STEP的ID，無則存STEP NAME
            when (spinner.selectedItemPosition) {
                0 -> {
                    return "Next${processList[processId + 1].stepName?.replace("STEP", "")?.toInt()}"
                }
                1 -> {
                    var len = stepName.length
                    var zero = ""
                    if (len < 4) {
                        for (i in len until 4) {
                            zero += "0"
                        }
                    }
                    var id = "STEP$zero$stepName"
                    for (i in 0 until processList.size) {
                        if (processList[i].stepName?.contains(id) == true) {
                            id = processList[i].id ?: ""
                            break
                        }
                    }
                    return id
                }
                2 -> {
                    var id = ""
                    for (i in 0 until processList.size) {
                        if (processList[i].Function == "END") {
                            id = processList[i].id ?: ""
                            break
                        }
                    }
                    return id
                }
            }
            return ""
        }
    }

    private fun check(sl:List<Spinner>,el:List<EditText>) {
        for (i in sl.indices) {
            when (sl[i].selectedItemPosition) {
                0 -> {
                    el[i].isEnabled = false
                    el[i].isFocusableInTouchMode = false
                }
                1 -> {
                    el[i].isEnabled = true
                    el[i].isFocusableInTouchMode = true
                }
                2 -> {
                    el[i].isEnabled = false
                    el[i].isFocusableInTouchMode = false
                }
            }
        }
    }

    inner class GotoItemSelected(editText: EditText):AdapterView.OnItemSelectedListener {

        private var editText = editText

        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            when (position) {
                0 -> {
                    //processId = position
                    editText.isEnabled = false
                    editText.isFocusableInTouchMode = false
                    editText.setText("")
                }
                1 -> {
                    editText.isEnabled = true
                    editText.isFocusableInTouchMode = true
                }
                2 -> {
                    editText.isEnabled = false
                    editText.isFocusableInTouchMode = false
                    editText.setText("")
                }
            }
        }
    }

    inner class textWatcher(val editText: EditText,val limit:Int): TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (limit > 0) {
                if (s?.indexOf(".") != -1) {
                    if (s?.substring(s.indexOf(".") + 1)?.length ?: 0 > limit) {
                        editText.setText(s?.substring(0, s.indexOf(".") + limit + 1))
                        editText.setSelection(editText.length())
                    }
                }
            } else {
                if (s?.contains(".") == true) {
                    editText.setText(s.substring(0,s.indexOf(".")))
                    editText.setSelection(editText.length())
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (hidden) {
            saveData()
            Log.i("condi", processList.toString())
        } else {
            mHandler.postDelayed({
                if (processList[processId].Function?.isNotEmpty() == true) {
                    initView()
                }
                Log.d("condi","visible")
            },500)
        }
    }

    override fun onPause() {
        super.onPause()
        saveData()
        Log.i("condi", processList.toString())
    }

}