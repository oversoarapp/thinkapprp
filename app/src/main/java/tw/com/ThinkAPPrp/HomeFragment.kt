package tw.com.thinkPower.ThinkAPPrp

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.Model.InfoModel
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException
import java.util.*
import kotlin.concurrent.timerTask

class HomeFragment : Fragment() {

    private var infoModel:InfoModel? = null
    private var flag = 1
    private var isTimerAvailable = true
    private var command = "00000008ChSTATUS"
    private var mStart = false
    private var socket = Socket()
    private var timer = Timer()
    private var mTimer = Timer()
    private var mHandler:Handler? = null
    private var sp:SharedPreferences?= null
    private var btnFlag = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        mHandler = Handler()
        sp = context?.getSharedPreferences("mstart",Context.MODE_PRIVATE)

        if (ip.isNotEmpty()) {
            timer.schedule(Task(),0,1500)
        }

        imageView3.setOnClickListener {
            if (btnFlag == 0) {
                homeMenu.visibility = View.VISIBLE
                btnFlag = 1
            } else {
                btnFlag = 0
                homeMenu.visibility = View.INVISIBLE
            }
        }

        mstartBtn.setOnClickListener {
            btnFlag = 0
            homeMenu.visibility = View.INVISIBLE
            if (sp?.getString("spinnerPosition1","")?.isNotEmpty() == true && ip.isNotEmpty()) {
//                progressBar3.visibility = View.VISIBLE
//                command = "00000017FILEREAD,DeviceINFOPath"
//                connectTask()
//            } else if (infoModel != null) {
                timer.cancel()
                isTimerAvailable = false
                mStart = true
                var data = "MSTART,,${dataToJson("")}"
                var zero = ""
                var hex = Integer.toHexString(data.length)
                var size = 8 - hex.length
                for (i in 0 until size) {
                    zero += "0"
                }
                command = zero+hex+data
                Log.i("mstart",command)
                progressBar3.visibility = View.VISIBLE
                connectTask()
            }
        }

        stopBtn.setOnClickListener {
            btnFlag = 0
            timer.cancel()
            isTimerAvailable = false
            mStart = true
            homeMenu.visibility = View.INVISIBLE
            progressBar3.visibility = View.VISIBLE
            command = "00000004STOP"
            connectTask()
        }

        pauseBtn.setOnClickListener {
            btnFlag = 0
            timer.cancel()
            isTimerAvailable = false
            mStart = true
            progressBar3.visibility = View.VISIBLE
            homeMenu.visibility = View.INVISIBLE
            command = "00000005PAUSE"
            connectTask()
        }

        resumeBtn.setOnClickListener {
            btnFlag = 0
            timer.cancel()
            isTimerAvailable = false
            mStart = true
            progressBar3.visibility = View.VISIBLE
            homeMenu.visibility = View.INVISIBLE
            command = "00000006RESUME"
            connectTask()
        }

    }

    private val reConnect = Runnable {
        if(homeIPstatus != null) {
            if (isTimerAvailable) {
                timer.cancel()
            }
            isTimerAvailable = false
            mStart = false
            homeIPstatus.setImageResource(R.drawable.ic_signal_y)
        }
        Log.i("reconnect","")
    }

    private val connectError = Runnable {
        if(homeIPstatus != null && progressBar3 != null) {
            ip = ""
//            if (isTimerAvailable) {
//                timer.cancel()
//            }
//            isTimerAvailable = false
//            mStart = false
            homeIPstatus.setImageResource(R.drawable.ic_signal_gray)
            progressBar3.visibility = View.INVISIBLE
            Toast.makeText(context, "連線錯誤，請確認網路正常，重新設置ip！", Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendTask() {
        val t = Thread(sendCommand)
        t.start()
        homeStatus.setImageResource(R.drawable.ic_wb_sunny_black_24dp)
        Log.i("startSendTask","")
    }

    private fun connectTask () {
        if (socket.isConnected) {
            var connect = Connect()
            connect.sockect(socket)
            connect.closeIOAndSocket()
        }
        val t = Thread(readData)
        t.start()
        Log.i("Connect","")
    }

    private val readData = Runnable {
        try {
            socket = Socket()
            socket.connect(InetSocketAddress (ip, 1688),3000)

            if (socket.isConnected) {
                mHandler?.post {
                    if (homeIPstatus != null) {
                        if (!isTimerAvailable && !mStart) {
                            isTimerAvailable = true
                            timer = Timer()
                            timer.schedule(Task(), 0, 1500)
                        }
                        homeIPstatus.setImageResource(R.drawable.ic_signal_green)
                        sendTask()
                        Log.i("sendTask", "")
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnect)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnect)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable {
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand(command)
            Log.i("sendding","")
        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnect)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
        Log.i("msg", msg)
        if (msg != "0000000fFile send fail." && !msg.contains("incorrect") && !msg.contains("error") && !msg.contains("The current operating state does not allow")) {
            when (command) {
                "00000008ChSTATUS" -> {
                    mHandler?.post {
                        if (homeStatus != null && progressBar3 != null) {
                            progressBar3?.visibility = View.INVISIBLE
                            homeStatus?.setImageResource(R.drawable.ic_wb_sunny_24dp)
                            setData(msg)
                        }
                    }
                    mTimer.schedule(timerTask {
                            mHandler?.post {
                                if (homeStatus != null) {
                                    homeStatus.setImageResource(R.drawable.ic_wb_sunny_black_24dp)
                                }
                            }
                    }, 500)
                }
//                "00000017FILEREAD,DeviceINFOPath" -> {
//                    if (msg.contains("{")) {
//                        val response = msg.substring(msg.indexOf("{"))
//                        val listType = object : TypeToken<InfoModel>() {}.type
//                        infoModel = Gson().fromJson(response, listType)
//                        var data =
//                            "MSTART,,${dataToJson(infoModel?.MachineModel ?: "")}"
//                        var zero = ""
//                        var hex = Integer.toHexString(data.length)
//                        var size = 8 - hex.length
//                        for (i in 0 until size) {
//                            zero += "0"
//                        }
//                        command = zero + hex + data
//                        mHandler?.post {
//                            connectTask()
//                        }
//                    } else {
//                        if (progressBar3 != null) {
//                            AlertDialog.Builder(context)
//                                .setMessage("something wrong,please try again")
//                                .show()
//                            progressBar3.visibility = View.INVISIBLE
//                        }
//                    }
//                }
                else -> {
                    mStart = false
                    mHandler?.post {
                        if (progressBar3 != null) {
                            if (!isTimerAvailable) {
                                isTimerAvailable = true
                                timer = Timer()
                                timer.schedule(Task(), 0, 3000)
                            }
                            progressBar3.visibility = View.INVISIBLE
                            Toast.makeText(context, "OK", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        } else {
            dialog(msg)
        }
    }

    private fun dialog(msg:String) {
        var msg = msg
        if (msg.contains("0")) {
            msg = msg.substring(8)
        }
        mHandler?.post {
            if (progressBar3 != null ) {
                progressBar3.visibility = View.INVISIBLE
                AlertDialog.Builder(context)
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton("確定") { d, w ->
                        if (!isTimerAvailable) {
                            isTimerAvailable = true
                            timer = Timer()
                            timer.schedule(Task(), 0, 1500)
                        }
                    }
                    .show()
            }
        }
    }

    private fun dataToJson(machine:String):String {
        var action = sp?.getString("spinnerPosition1","")
        var model = sp?.getString("spinnerPosition2","")
        var vset = parse(sp?.getString("voltageSet","")?:"")
        var iset = parse(sp?.getString("currentSet","")?:"")
        var pset = sp?.getString("powerSet","")?:""
        var timeset = parse(sp?.getString("timeSet","")?:"")
        var recordtimeset = parse(sp?.getString("recordTime","")?:"")
        var ecset = parse(sp?.getString("ec","")?:"")
        var evset = parse(sp?.getString("ev","")?:"")
        var mah = sp?.getString("mah","")?:""
        var wh = sp?.getString("wh","")?:""
//        var evcompset = when (action) {
//            "CHARGE" -> ">="
//            "DISCHARGE" -> "<="
//            "REST" -> "None"
//            else -> ""
//        }
        return "{\"Machine\":\"xxxxx\",\"ProcessName\":\"ManualReservedName.json\",\"Function-1\":\"NA\",\"LoopST1-1\":0,\"LoopST2-1\":0,\"LoopEND1-1\":0,\"LoopEND2-1\":0,\"Jump-1\":0,\"STEPAction-1\":\"$action\",\"STEPModel-1\":\"$model\",\"Vset-1\":$vset,\"Iset-1\":$iset,\"Pset-1\":$pset,\"Timeset-1\":$timeset,\"RecordTimeset-1\":$recordtimeset,\"mAhset-1\":$mah,\"mAhCdtset-1\":0,\"Whset-1\":$wh,\"WhCdtset-1\":0,\"EVset-1\":$evset,\"EVCompset-1\":\"\",\"EVCdtset-1\":0,\"ECset-1\":$ecset,\"ECCdtset-1\":0,\"mdVset-1\":0,\"mdVCdtset-1\":0,\"ETset-1\":0,\"ETCompset-1\":\"\",\"ETCdtset-1\":0,\"Function-2\":\"END\",\"LoopST1-2\":0,\"LoopST2-2\":0,\"LoopEND1-2\":0,\"LoopEND2-2\":0,\"Jump-2\":0,\"STEPAction-2\":\"\",\"STEPModel-2\":\"\",\"Vset-2\":0,\"Iset-2\":0,\"Pset-2\":0,\"Timeset-2\":0,\"RecordTimeset-2\":0,\"mAhset-2\":0,\"mAhCdtset-2\":0,\"Whset-2\":0,\"WhCdtset-2\":0,\"EVset-2\":0,\"EVCompset-2\":\"\",\"EVCdtset-2\":0,\"ECset-2\":0,\"ECCdtset-2\":0,\"mdVset-2\":0,\"mdVCdtset-2\":0,\"ETset-2\":0,\"ETCompset-2\":\"\",\"ETCdtset-2\":0}"
        Log.d("mstart","{\"Machine\":\"xxxxx\",\"ProcessName\":\"ManualReservedName.json\",\"Function-1\":\"NA\",\"LoopST1-1\":0,\"LoopST2-1\":0,\"LoopEND1-1\":0,\"LoopEND2-1\":0,\"Jump-1\":0,\"STEPAction-1\":\"$action\",\"STEPModel-1\":\"$model\",\"Vset-1\":$vset,\"Iset-1\":$iset,\"Pset-1\":$pset,\"Timeset-1\":$timeset,\"RecordTimeset-1\":$recordtimeset,\"mAhset-1\":$mah,\"mAhCdtset-1\":0,\"Whset-1\":$wh,\"WhCdtset-1\":0,\"EVset-1\":$evset,\"EVCompset-1\":\"\",\"EVCdtset-1\":0,\"ECset-1\":$ecset,\"ECCdtset-1\":0,\"mdVset-1\":0,\"mdVCdtset-1\":0,\"ETset-1\":0,\"ETCompset-1\":\"\",\"ETCdtset-1\":0,\"Function-2\":\"END\",\"LoopST1-2\":0,\"LoopST2-2\":0,\"LoopEND1-2\":0,\"LoopEND2-2\":0,\"Jump-2\":0,\"STEPAction-2\":\"\",\"STEPModel-2\":\"\",\"Vset-2\":0,\"Iset-2\":0,\"Pset-2\":0,\"Timeset-2\":0,\"RecordTimeset-2\":0,\"mAhset-2\":0,\"mAhCdtset-2\":0,\"Whset-2\":0,\"WhCdtset-2\":0,\"EVset-2\":0,\"EVCompset-2\":\"\",\"EVCdtset-2\":0,\"ECset-2\":0,\"ECCdtset-2\":0,\"mdVset-2\":0,\"mdVCdtset-2\":0,\"ETset-2\":0,\"ETCompset-2\":\"\",\"ETCdtset-2\":0}")
    }

    private fun parse (text:String):String {
        var convert: Double
        return if (text != "0" && text.isNotEmpty()) {
            convert = text.toDouble() * 1000
            convert.toInt().toString()
        } else {
            text
        }
        return text
    }

    private fun setData(msg:String) {
        var list = msg.split(",")
        if (list.size > 1 ) {
            var stepTime = list[6].substring(list[6].indexOf(":") + 1)
            var totalTime = list[7].substring(list[7].indexOf(":") + 1)
            var stepIndex = list[8].substring(list[8].indexOf(":") + 1)
            var stepAction = list[9].substring(list[9].indexOf(":") + 1)
            var stepModel = list[10].substring(list[10].indexOf(":") + 1)
            var loop1 = list[11].substring(list[11].indexOf(":") + 1)
            var loop2 = list[12].substring(list[12].indexOf(":") + 1)
            var processName = list[14].substring(list[14].indexOf(":") + 1)
            var info = list[15].substring(list[15].indexOf(":") + 1)

            if (stepAction.isNotEmpty()) {
                if (stepAction.contains("@") && stepAction.contains("End")) {
                    if (stepAction.substring(
                            stepAction.indexOf("@"),
                            stepAction.indexOf("End")
                        ).isNotEmpty()
                    ) {
                        stepAction = stepAction.replace("@", "")
                        stepAction = stepAction.replace("End", "")
                    } else {
                        stepAction = stepAction.replace("@", "")
                    }
                } else if (stepAction.contains("@")) {
                    stepAction = stepAction.replace("@", "")
                }
            } else {
                stepAction = ""
            }

            if (stepModel.isEmpty()) {
                stepModel = ""
            }

            if (stepIndex.contains(".")) {
                stepIndex = stepIndex.substring(0, stepAction.indexOf("."))
            }


//            parse(true, list[0], 4, mainTv)
//            parse(true, list[1], 4, mainTv2)
//            parse(false, list[2], 6, mainTv3)
//            parse(false, list[3], 1, mainTv4)
//            parse(false, list[4], 3, mainTv5)
            mainTv?.text = String.format("%.4f",(list[0].substring(list[0].indexOf(":")+1)).toDouble()/1000)
            mainTv2?.text = String.format("%.4f",(list[1].substring(list[1].indexOf(":")+1)).toDouble()/1000)
            mainTv3?.text = String.format("%.6f",(list[2].substring(list[2].indexOf(":")+1)).toDouble())
            mainTv4?.text = String.format("%.1f",(list[3].substring(list[3].indexOf(":")+1)).toDouble())
            mainTv5?.text = String.format("%.3f",(list[4].substring(list[4].indexOf(":")+1)).toDouble())
            mainTv6?.text = String.format("%.6f",(list[5].substring(list[5].indexOf(":")+1)).toDouble())
            mainTv7?.text = convertTime(stepTime.toDouble())
            mainTv8?.text = convertTime(totalTime.toDouble())
            mainTv9?.text = stepIndex
            mainTv10?.text = stepAction
            mainTv11?.text = stepModel
            mainTv12?.text = loop1
            mainTv13?.text = loop2
            mainTv14?.text = processName.replace(".json", "")
            mainTv15?.text = info
        }
    }

    private fun convertTime(value:Double):String {
        if (value != 0.0 ) {
            var h = String.format("%04d",(value / 3600000).toInt())
            var m = String.format("%02d",((value % 3600000) / 60000).toInt())
            var s = String.format("%02.1f",((value % 3600000) % 60000) / 1000.0)
//            var bd = BigDecimal(s)
//            bd = bd.setScale(1, RoundingMode.HALF_UP)
            return "${h}H${m}M${s}S"
        } else {
            return "0000H00M00.0S"
        }
    }

//    private fun parse (div:Boolean,text:String,index:Int,textView: TextView) {
//        //index為要取小數點後的幾位
//        var mText = text
//        mText = mText.substring(mText.indexOf(":")+1)
//        if (mText == "0.0" || mText == "0") {
//            textView.text = mText
//        } else {
//            if (div) {
//                mText = (mText.toDouble() / 1000).toString()
//            }
//            if (mText.substring(mText.indexOf(".")+1).length > index ) {
//                val bd = BigDecimal(mText)
//                textView.text = bd.setScale(index,1).toString()
//            } else {
//                textView.text = mText
//            }
//        }
//    }

    inner class Task : TimerTask() {
        override fun run() {
            mHandler?.post {
                if (progressBar3 != null) {
                    progressBar3.visibility = View.VISIBLE
                    command = "00000008ChSTATUS"
                    connectTask()
                    Log.i("startTimer","")
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (socket.isConnected) {
            var c = Connect()
            c.sockect(socket)
            c.closeIOAndSocket()
        }
        mHandler = null
        timer.cancel()
        Log.d("timerCancel","")
    }
}