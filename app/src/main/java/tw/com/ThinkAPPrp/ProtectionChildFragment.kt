package tw.com.thinkPower.ThinkAPPrp

import android.content.SharedPreferences
import android.icu.math.BigDecimal
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.child_fragment_protection.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.Model.SetInfoModel
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException
import kotlin.math.round


class ProtectionChildFragment: Fragment() {

    private var socket = Socket()
    private var mHandler = Handler()
    private var spinner = ""
    private var command = ""
    private var flag = 1
    private var default = ""
    private var sp: SharedPreferences? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.child_fragment_protection, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun initView() {

        protectionEt1.addTextChangedListener(textWatcher(protectionEt1,3))
        protectionEt2.addTextChangedListener(textWatcher(protectionEt2,3))
        protectionEt3.addTextChangedListener(textWatcher(protectionEt3,3))
        protectionEt4.addTextChangedListener(textWatcher(protectionEt4,3))
        protectionEt5.addTextChangedListener(textWatcher(protectionEt5,3))
        protectionEt6.addTextChangedListener(textWatcher(protectionEt6,3))
        protectionEt7.addTextChangedListener(textWatcher(protectionEt7,1))
        protectionEt8.addTextChangedListener(textWatcher(protectionEt8,0))
        protectionEt9.addTextChangedListener(textWatcher(protectionEt9,0))
        protectionEt10.addTextChangedListener(textWatcher(protectionEt10,0))

        protectionSet.setOnClickListener {
            val list = listOf(protectionEt1,protectionEt2,protectionEt3,protectionEt4,protectionEt5,protectionEt6,protectionEt7,protectionEt8,protectionEt9,protectionEt10)
            var isEmpty = false
            for ( i in list.indices) {
                if (list[i].text.isEmpty()) {
                    isEmpty = true
                }
            }
            if (!isEmpty) {
                var tmp = "FILESEND,ProtectionPath,${dataToJson()}"
                var zero = ""
                var hex = Integer.toHexString(tmp.length)
                var size = 8 - hex.length
                for (i in 0 until size) {
                    zero += "0"
                }
                command = zero + hex + tmp
                Log.d("command", command)
                if (ip.isNotEmpty()) {
                    connectTask()
                }
            } else {
                Toast.makeText(context,"欄位皆不得為空",Toast.LENGTH_SHORT).show()
            }
        }

        protectionRead.setOnClickListener {
            command = "00000017FILEREAD,ProtectionPath"
            if (ip.isNotEmpty()) {
                connectTask()
            }
        }

        protectionSwitch.setOnCheckedChangeListener {view,checked ->
            if (checked) {
                command = "0000001DFILEREAD,ProtectReplyFilePath"
                if (ip.isNotEmpty()) {
                    connectTask()
                } else {
                    protectionSwitch.isChecked = false
                }
            }
        }

        protectionSpinner.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                when (position) {
                    0-> spinner = "chargeVF"
                    1-> spinner = "dischargeVF"
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun dataToJson ():String {
        var ovp = convert(true,protectionEt1.text.toString())
        var uvp = convert(true,protectionEt2.text.toString())
        var cocp = convert(true,protectionEt3.text.toString())
        var cucp = convert(true,protectionEt4.text.toString())
        var docp = convert(true,protectionEt5.text.toString())
        var ducp = convert(true,protectionEt6.text.toString())
        var otp = protectionEt7.text.toString()
        var alarmDelay = protectionEt8.text.toString()
        var ecdelay = protectionEt9.text.toString()
        var evdelay = protectionEt10.text.toString()
        return "{\"OVP\":$ovp,\"UVP\":$uvp,\"COCP\":$cocp,\"CUCP\":$cucp,\"DOCP\":$docp,\"DUCP\":$ducp,\"OTP\":$otp,\"AlarmDelay\":$alarmDelay,\"ECDelay\":$ecdelay,\"EVDelay\":$evdelay,\"ChDchVFBSW\":\"$spinner\"}"
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun convert (time:Boolean, text:String):String {
        if (text != "0" && text != "0.0" && text != "") {
            return when (time) {
                true -> {
                    if (text.indexOf(".") != -1) {
                        val bd = BigDecimal(text)
                        var s = bd.setScale(3,1)
                        (s.toDouble()*1000).toInt().toString()
                    } else {
                        (text.toInt() * 1000).toString()
                    }
                }
                false -> {
                    if (text.indexOf(".") != -1) {
                        val bd = BigDecimal(text)
                        var s = bd.setScale(1,1)
                        s.toString()
                    } else {
                        text
                    }
                }
            }
        } else {
            return text
        }
    }

//    private val reConnectToast = Runnable {
//        Toast.makeText(context,"連線錯誤，嘗試連線中！", Toast.LENGTH_SHORT).show()
//    }

    private val connectError = Runnable {
        ip = ""
        Toast.makeText(context,"連線錯誤，請確認網路正常，重新設置ip！",Toast.LENGTH_SHORT).show()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun sendTask() {
        val t = Thread(sendCommand)
        t.start()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun connectTask () {
        if (socket.isConnected) {
            var connect = Connect()
            connect.sockect(socket)
            connect.closeIOAndSocket()
        }
        if (progressBar6 != null) {
            progressBar6.visibility = View.VISIBLE
        }
        val t = Thread(readData)
        t.start()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private val readData = Runnable {
        try {
            socket = Socket()
            socket.connect(InetSocketAddress (ip, 1688),3000)

            if (socket.isConnected) {
                sendTask()
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
//                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
//                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private val sendCommand = Runnable {
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand(command)
        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
//                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
        Log.d("msg", msg)
        if (!msg.contains("failed") && !msg.contains("does not exist") && !msg.contains("ERROR") && msg.isNotEmpty()) {
            when (command) {
                "0000001DFILEREAD,ProtectReplyFilePath"-> {
                    //取得原廠設定資料後，執行set。
                    var data = "FILESEND,ProtectionPath,${msg.substring(8).replace(" ","")}"
                    default = msg.substring(8).replace(" ","")
                    var zero = ""
                    var hex = Integer.toHexString(data.length)
                    var size = 8 - hex.length
                    for (i in 0 until size) {
                        zero += "0"
                    }
                    command = zero+hex+data
                    mHandler.post {
                        connectTask()
                    }
                }
                "00000017FILEREAD,ProtectionPath" -> {
                    mHandler.post{
                        if (progressBar6 != null) {
                            progressBar6.visibility = View.INVISIBLE
                            parse(msg.substring(8).replace(" ", ""))
                        }
                    }
                }
                else -> {
                    if (command.contains("FILESEND")) {
                        mHandler.post{
                            if (default.isNotEmpty() && command.contains(default)) {
                                protectionSwitch.isChecked = false
                                parse(default)
                            }
                        }
                    }
                    mHandler.post {
                        if (progressBar6 != null) {
                            Toast.makeText(context, "OK", Toast.LENGTH_SHORT).show()
                            progressBar6.visibility = View.INVISIBLE
                        }
                    }
                }
            }
        } else {
            if (msg.contains("0")) {
                msg = msg.substring(8)
            }
            mHandler.post {
                if (progressBar6 != null) {
                    AlertDialog.Builder(context!!)
                        .setMessage(msg)
                        .show()
                    progressBar6.visibility = View.INVISIBLE
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun parse(msg:String) {
        var listType = object : TypeToken<SetInfoModel>() {}.type
        var model: SetInfoModel = Gson().fromJson(msg,listType)
        setData(true,model.OVP?:0,3,protectionEt1)
        setData(true,model.UVP?:0,3,protectionEt2)
        setData(true,model.COCP?:0,3,protectionEt3)
        setData(true,model.CUCP?:0,3,protectionEt4)
        setData(true,model.DOCP?:0,3,protectionEt5)
        setData(true,model.DUCP?:0,3,protectionEt6)
        protectionEt7.setText("${model.OTP?:0}")
        setData(false,model.AlarmDelay?:0,0,protectionEt8)
        setData(false,model.ECDelay?:0,0,protectionEt9)
        setData(false,model.EVDelay?:0,0,protectionEt10)
        if (model.ChDchVFBSW == "chargeVF") {
            protectionSpinner.setSelection(0)
        } else {
            protectionSpinner.setSelection(1)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun setData (div:Boolean, value:Int, index: Int, view:EditText) {
        var m = value.toString()
        if (value != 0) {
            if (div) {
                m = (value / 1000.0).toString()
            }
            if (m.contains(".")) {
                if (m.length == (m.substring(0,m.indexOf(".")+2)).length && m.substring(m.indexOf(".")+1) == "0") {
                    val bd = BigDecimal(m)
                    m = bd.setScale(0, 1).toString()
                } else {
                    if (m.substring(m.indexOf(".") + 1).length > index) {
                        val bd = BigDecimal(m)
                        m = bd.setScale(index, 1).toString()
                    }
                }
            }
        }
        view.setText(m)
    }

    private fun getData() {
        if (sp?.getString("otp","")?.isNotEmpty() == true){
            protectionEt1.setText(sp?.getString("ovp",""))
            protectionEt2.setText(sp?.getString("uvp",""))
            protectionEt3.setText(sp?.getString("cocp",""))
            protectionEt4.setText(sp?.getString("cucp",""))
            protectionEt5.setText(sp?.getString("docp",""))
            protectionEt6.setText(sp?.getString("ducp",""))
            protectionEt7.setText(sp?.getString("otp",""))
            protectionEt8.setText(sp?.getString("alarmdelay",""))
            protectionEt9.setText(sp?.getString("ecdelay",""))
            protectionEt10.setText(sp?.getString("evdelay",""))
            protectionSpinner.setSelection(sp?.getInt("chg/dchg",0)?:0)
        }
    }

    private fun saveData() {
        sp?.edit()?.putString("ovp",protectionEt1.text.toString())?.apply()
        sp?.edit()?.putString("uvp",protectionEt2.text.toString())?.apply()
        sp?.edit()?.putString("cocp",protectionEt3.text.toString())?.apply()
        sp?.edit()?.putString("cucp",protectionEt4.text.toString())?.apply()
        sp?.edit()?.putString("docp",protectionEt5.text.toString())?.apply()
        sp?.edit()?.putString("ducp",protectionEt6.text.toString())?.apply()
        sp?.edit()?.putString("otp",protectionEt7.text.toString())?.apply()
        sp?.edit()?.putString("alarmdelay",protectionEt8.text.toString())?.apply()
        sp?.edit()?.putString("ecdelay",protectionEt9.text.toString())?.apply()
        sp?.edit()?.putString("evdelay",protectionEt10.text.toString())?.apply()
        sp?.edit()?.putInt("chg/dchg",protectionSpinner.selectedItemPosition)?.apply()
    }

    inner class textWatcher(val editText: EditText,val limit:Int): TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (limit > 0) {
                if (s?.indexOf(".") != -1) {
                    if (s?.substring(s.indexOf(".") + 1)?.length ?: 0 > limit) {
                        editText.setText(s?.substring(0, s.indexOf(".") + limit + 1))
                        editText.setSelection(editText.length())
                    }
                }
            } else {
                if (s?.contains(".") == true) {
                    editText.setText(s.substring(0,s.indexOf(".")))
                    editText.setSelection(editText.length())
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    }

    override fun onDestroy() {
        super.onDestroy()
        if (socket.isConnected) {
            var c = Connect()
            c.sockect(socket)
            c.closeIOAndSocket()
        }
    }
}