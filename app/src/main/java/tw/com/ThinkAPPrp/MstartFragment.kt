package tw.com.thinkPower.ThinkAPPrp

import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_mstart.*
import kotlinx.android.synthetic.main.fragment_mstart.mstartBtn
import kotlinx.android.synthetic.main.fragment_mstart.pauseBtn
import kotlinx.android.synthetic.main.fragment_mstart.resumeBtn
import kotlinx.android.synthetic.main.fragment_mstart.stopBtn
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.Model.InfoModel
import java.io.IOException
import java.math.BigDecimal
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException

class MstartFragment : Fragment() {

    private var command = ""
    private var infoModel: InfoModel? = null
    private var sp : SharedPreferences? = null
    private var socket = Socket()
    private var mHandler = Handler()
    private var flag = 1
    private var btnFlag = 0
    private var spinnerFlag = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_mstart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        mstartTv1.addTextChangedListener(textWatcher(mstartTv1,3))//Voltage
        mstartTv2.addTextChangedListener(textWatcher(mstartTv2,3))//current
        mstartTv3.addTextChangedListener(textWatcher(mstartTv3,3))//power
        mstartTv4.addTextChangedListener(textWatcher(mstartTv4,1))//time
        mstartTv5.addTextChangedListener(textWatcher(mstartTv5,1))//record
        mstartTv6.addTextChangedListener(textWatcher(mstartTv6,3))//ec
        mstartTv7.addTextChangedListener(textWatcher(mstartTv7,3))//ev
        mstartTv8.addTextChangedListener(textWatcher(mstartTv8,0))//mah
        mstartTv9.addTextChangedListener(textWatcher(mstartTv9,3))//wh

        sp = context?.getSharedPreferences("mstart",Context.MODE_PRIVATE)
        if (sp != null && sp?.getString("voltageSet","")?.isNotEmpty() == true) {
            spinnerFlag = 1

            val actionList = context?.resources?.getStringArray(R.array.mstart_action)
            actionList?.mapIndexed { index, s ->
                if (sp?.getString("spinnerPosition1","") == s) {
                    mStartSpinner1.setSelection(index)
                }
            }

            var modelList = listOf<String>()
            when (mStartSpinner1.selectedItem) {
                "CHARGE" -> modelList = listOf("CC","CC-CV")
                "DISCHARGE" -> modelList = listOf("CC","CC-CV","CC-CP")
                "REST" -> {
                    modelList = listOf("")
                }
            }
            val adapter = ArrayAdapter(
                context!!,
                android.R.layout.simple_spinner_dropdown_item,
                modelList
            )
            mStartSpinner2.adapter = adapter
            modelList.mapIndexed { index, s ->
                if (sp?.getString("spinnerPosition2","") == s) {
                    mStartSpinner2.setSelection(index)
                }
            }

            mstartTv1.setText(sp?.getString("voltageSet",""))
            mstartTv2.setText(sp?.getString("currentSet",""))
            mstartTv3.setText(sp?.getString("powerSet",""))
            mstartTv4.setText(sp?.getString("timeSet",""))
            mstartTv5.setText(sp?.getString("recordTime",""))
            mstartTv6.setText(sp?.getString("ec",""))
            mstartTv7.setText(sp?.getString("ev",""))
            mstartTv8.setText(sp?.getString("mah",""))
            mstartTv9.setText(sp?.getString("wh",""))
        }

        if (ip.isNotEmpty()){
            mstartIPstatus.setImageResource(R.drawable.ic_signal_green)
        }

        imageView.setOnClickListener {
            if (btnFlag == 0) {
                mstartMenu.visibility = View.VISIBLE
                btnFlag = 1
            } else {
                btnFlag = 0
                mstartMenu.visibility = View.INVISIBLE
            }
        }

        mstartBtn.setOnClickListener {
            btnFlag = 0
            mstartMenu.visibility = View.INVISIBLE
            if (GlobalVar.ip.isNotEmpty()) {
//                if (infoModel == null) {
//                    command = "00000017FILEREAD,DeviceINFOPath"
//                    connectTask()
//                } else {
                progressBar5.visibility = View.VISIBLE
                var data = "MSTART,,${dataToJson()}"
                var zero = ""
                var hex = Integer.toHexString(data.length)
                var size = 8 - hex.length
                for (i in 0 until size) {
                    zero += "0"
                }
                command = zero+hex+data
                Log.i("mstart",command)
                connectTask()
            }
        }

        stopBtn.setOnClickListener {
            btnFlag = 0
            mstartMenu.visibility = View.INVISIBLE
            command = "00000004STOP"
            if (GlobalVar.ip.isNotEmpty()) {
                progressBar5.visibility = View.VISIBLE
                connectTask()
            }
        }

        pauseBtn.setOnClickListener {
            btnFlag = 0
            mstartMenu.visibility = View.INVISIBLE
            command = "00000005PAUSE"
            if (GlobalVar.ip.isNotEmpty()) {
                progressBar5.visibility = View.VISIBLE
                connectTask()
            }
        }

        resumeBtn.setOnClickListener {
            btnFlag = 0
            mstartMenu.visibility = View.INVISIBLE
            command = "00000006RESUME"
            if (GlobalVar.ip.isNotEmpty()) {
                progressBar5.visibility = View.VISIBLE
                connectTask()
            }
        }

        mStartSpinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (spinnerFlag == 0) {
                    when (position) {
                        0 -> {
                            val model = arrayListOf("CC", "CC-CV")
                            val adapter = ArrayAdapter(
                                context!!,
                                android.R.layout.simple_spinner_dropdown_item,
                                model
                            )
                            mStartSpinner2.adapter = adapter
                            etSetting(
                                listOf(
                                    mstartTv1,
                                    mstartTv2,
                                    mstartTv3,
                                    mstartTv4,
                                    mstartTv5,
                                    mstartTv6,
                                    mstartTv7,
                                    mstartTv8,
                                    mstartTv9
                                ), true
                            )
                        }
                        1 -> {
                            val model = arrayListOf("CC", "CC-CV", "CC-CP")
                            val adapter = ArrayAdapter(
                                context!!,
                                android.R.layout.simple_spinner_dropdown_item,
                                model
                            )
                            mStartSpinner2.adapter = adapter
                            etSetting(
                                listOf(
                                    mstartTv1,
                                    mstartTv2,
                                    mstartTv3,
                                    mstartTv4,
                                    mstartTv5,
                                    mstartTv6,
                                    mstartTv7,
                                    mstartTv8,
                                    mstartTv9
                                ), true
                            )
                        }
                        2 -> {
                            val model = arrayListOf("")
                            val adapter = ArrayAdapter(
                                context!!,
                                android.R.layout.simple_spinner_dropdown_item,
                                model
                            )
                            mStartSpinner2.adapter = adapter
                        }
                    }
                }
                spinnerFlag = 0
            }
        }

        mStartSpinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                mstartTv1.isEnabled = true
                mstartTv3.isEnabled = true
                mstartTv7.isEnabled = true
                mstartTv6.isEnabled = true
                mstartTv1.background = resources.getDrawable(R.drawable.mstart_outside)
                mstartTv3.background = resources.getDrawable(R.drawable.mstart_outside)
                mstartTv7.background = resources.getDrawable(R.drawable.mstart_outside)
                mstartTv6.background = resources.getDrawable(R.drawable.mstart_outside)
                when (mStartSpinner2.selectedItem) {
                    "" -> {
                        etSetting(
                            listOf(
                                mstartTv1,
                                mstartTv2,
                                mstartTv3,
                                mstartTv6,
                                mstartTv7,
                                mstartTv8,
                                mstartTv9
                            ), false
                        )
                    }
                    "CC" -> {
                        if (mStartSpinner1.selectedItemPosition == 0 || mStartSpinner1.selectedItemPosition == 1) {
                            mstartTv1.setText("0")
                            mstartTv3.setText("0")
                            mstartTv6.setText("0")
                            mstartTv6.isEnabled = false
                            mstartTv1.isEnabled = false
                            mstartTv3.isEnabled = false
                            mstartTv1.background = resources.getDrawable(R.drawable.grey_padding_outside)
                            mstartTv3.background = resources.getDrawable(R.drawable.grey_padding_outside)
                            mstartTv6.background = resources.getDrawable(R.drawable.grey_padding_outside)
                        }
//                        } else {
//                            mstartTv6.isEnabled = true
//                            mstartTv6.background = resources.getDrawable(R.drawable.mstart_outside)
//                        }
                    }
                    "CC-CV" -> {
                        if (mStartSpinner1.selectedItemPosition == 0 || mStartSpinner1.selectedItemPosition == 1) {
                            mstartTv7.setText("0")
                            mstartTv3.setText("0")
                            mstartTv3.isEnabled = false
                            mstartTv7.isEnabled = false
                            mstartTv3.background = resources.getDrawable(R.drawable.grey_padding_outside)
                            mstartTv7.background = resources.getDrawable(R.drawable.grey_padding_outside)
                        }
//                        } else {
//                            mstartTv7.isEnabled = true
//                            mstartTv7.background = resources.getDrawable(R.drawable.mstart_outside)
//                        }
                    }
                    "CC-CP" -> {
                        if (mStartSpinner1.selectedItemPosition == 1) {
                            mstartTv6.setText("0")
                            mstartTv1.setText("0")
                            mstartTv1.isEnabled = false
                            mstartTv6.isEnabled = false
                            mstartTv1.background = resources.getDrawable(R.drawable.grey_padding_outside)
                            mstartTv6.background = resources.getDrawable(R.drawable.grey_padding_outside)
                        }
//                        } else {
//                            mstartTv6.isEnabled = true
//                            mstartTv6.background = resources.getDrawable(R.drawable.mstart_outside)
//                        }
                    }
                }
            }
        }
    }

    private fun etSetting (list: List<EditText>,bool:Boolean) {
        for (i in list.indices) {
            list[i].isEnabled = bool
            list[i].isFocusableInTouchMode = bool
            if (bool) {
                list[i].background = resources.getDrawable(R.drawable.mstart_outside)
            } else {
                list[i].setText("0")
                list[i].background = resources.getDrawable(R.drawable.grey_padding_outside)
            }
        }
        if(bool) {
            mStartSpinner2.background = resources.getDrawable(R.drawable.mstart_outside)
        } else {
            mStartSpinner2.background = resources.getDrawable(R.drawable.grey_padding_outside)
        }
        mStartSpinner2.isEnabled = bool
    }

    private val reConnectToast = Runnable {
        if (mstartIPstatus != null) {
            mstartIPstatus.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (mstartIPstatus != null) {
            ip = ""
            mstartIPstatus.setImageResource(R.drawable.ic_signal_gray)
            Toast.makeText(context, "連線錯誤，請確認網路正常，重新設置ip！", Toast.LENGTH_SHORT).show()
        }
    }

    private fun sendTask() {
        val t = Thread(sendCommand)
        t.start()
    }

    private fun connectTask () {
        if (socket.isConnected) {
            var connect = Connect()
            connect.sockect(socket)
            connect.closeIOAndSocket()
        }
        val t = Thread(readData)
        t.start()
    }

    private val readData = Runnable {
        try {
            socket = Socket()
            socket.connect(InetSocketAddress (GlobalVar.ip, 1688),3000)

            if (socket.isConnected) {
                if (mstartIPstatus != null) {
                    mstartIPstatus.setImageResource(R.drawable.ic_signal_green)
                    sendTask()
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable {
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand(command)
        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
        Log.d("msg", msg)
        if (msg.contains("OK") && !msg.contains("incorrect") && !msg.contains("File send fail")) {
//            when(command) {
//                "00000017FILEREAD,DeviceINFOPath" -> {
//                    if (msg.contains("{")) {
//                        val response = msg.substring(msg.indexOf("{"))
//                        val listType = object : TypeToken<InfoModel>() {}.type
//                        infoModel = Gson().fromJson(response, listType)
//                        var data = "MSTART,,${dataToJson(infoModel?.MachineModel ?: "")}"
//                        var zero = ""
//                        var hex = Integer.toHexString(data.length)
//                        var size = 8 - hex.length
//                        for (i in 0 until size) {
//                            zero += "0"
//                        }
//                        command = zero + hex + data
//                        mHandler.post {
//                            connectTask()
//                        }
//                    }
//                }
//                else -> {
            mHandler.post {
                if(progressBar5 != null) {
                    progressBar5.visibility = View.INVISIBLE
                    Toast.makeText(context, "OK", Toast.LENGTH_LONG).show()
                }
            }
        } else {
            if (msg.length > 8) {
                msg = msg.substring(8)
            }
            mHandler.post {
                if (progressBar5 != null) {
                    AlertDialog.Builder(context)
                        .setMessage(msg)
                        .show()
                    progressBar5.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun dataToJson ():String {
        var action = mStartSpinner1.selectedItem.toString()
        var model = mStartSpinner2.selectedItem.toString()
        var vset = parse(mstartTv1.text.toString())
        var iset = parse(mstartTv2.text.toString())
        var pset = mstartTv3.text.toString()
        var timeset = parse(mstartTv4.text.toString())
        var recordtimeset = parse(mstartTv5.text.toString())
        var ecset = parse(mstartTv6.text.toString())
        var evset = parse(mstartTv7.text.toString())
        var mah = mstartTv8.text.toString().toInt().toString()
        var wh = mstartTv9.text.toString()
        var evcompset = when (action) {
            "CHARGE" -> ">="
            "DISCHARGE" -> "<="
            "REST" -> "None"
            else -> ""
        }
        return "{\"Machine\":\"xxxxx\",\"ProcessName\":\"ManualReservedName.json\",\"Function-1\":\"NA\",\"LoopST1-1\":0,\"LoopST2-1\":0,\"LoopEND1-1\":0,\"LoopEND2-1\":0,\"Jump-1\":0,\"STEPAction-1\":\"$action\",\"STEPModel-1\":\"$model\",\"Vset-1\":$vset,\"Iset-1\":$iset,\"Pset-1\":$pset,\"Timeset-1\":$timeset,\"RecordTimeset-1\":$recordtimeset,\"mAhset-1\":$mah,\"mAhCdtset-1\":0,\"Whset-1\":$wh,\"WhCdtset-1\":0,\"EVset-1\":$evset,\"EVCompset-1\":\"\",\"EVCdtset-1\":0,\"ECset-1\":$ecset,\"ECCdtset-1\":0,\"mdVset-1\":0,\"mdVCdtset-1\":0,\"ETset-1\":0,\"ETCompset-1\":\"\",\"ETCdtset-1\":0,\"Function-2\":\"END\",\"LoopST1-2\":0,\"LoopST2-2\":0,\"LoopEND1-2\":0,\"LoopEND2-2\":0,\"Jump-2\":0,\"STEPAction-2\":\"\",\"STEPModel-2\":\"\",\"Vset-2\":0,\"Iset-2\":0,\"Pset-2\":0,\"Timeset-2\":0,\"RecordTimeset-2\":0,\"mAhset-2\":0,\"mAhCdtset-2\":0,\"Whset-2\":0,\"WhCdtset-2\":0,\"EVset-2\":0,\"EVCompset-2\":\"\",\"EVCdtset-2\":0,\"ECset-2\":0,\"ECCdtset-2\":0,\"mdVset-2\":0,\"mdVCdtset-2\":0,\"ETset-2\":0,\"ETCompset-2\":\"\",\"ETCdtset-2\":0}"
    }

    private fun parse (text:String):String {
        var convert: Double
        return if (text != "0" && text.isNotEmpty()) {
            convert = text.toDouble() * 1000
            convert.toInt().toString()
        } else {
            text
        }
        return text
    }

    private fun saveData() {
        sp?.edit()?.putString("spinnerPosition1",mStartSpinner1.selectedItem.toString())?.apply()
        sp?.edit()?.putString("spinnerPosition2",mStartSpinner2.selectedItem.toString())?.apply()
        sp?.edit()?.putString("voltageSet",mstartTv1.text.toString())?.apply()
        sp?.edit()?.putString("currentSet",mstartTv2.text.toString())?.apply()
        sp?.edit()?.putString("powerSet",mstartTv3.text.toString())?.apply()
        sp?.edit()?.putString("timeSet",mstartTv4.text.toString())?.apply()
        sp?.edit()?.putString("recordTime",mstartTv5.text.toString())?.apply()
        sp?.edit()?.putString("ec",mstartTv6.text.toString())?.apply()
        sp?.edit()?.putString("ev",mstartTv7.text.toString())?.apply()
        sp?.edit()?.putString("mah",mstartTv8.text.toString().toInt().toString())?.apply()
        sp?.edit()?.putString("wh",mstartTv9.text.toString())?.apply()
    }

    inner class textWatcher(val editText: EditText,val limit:Int): TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (limit > 0) {
                if (s?.indexOf(".") != -1) {
                    if (s?.substring(s.indexOf(".") + 1)?.length ?: 0 > limit) {
                        editText.setText(s?.substring(0, s.indexOf(".") + limit + 1))
                        editText.setSelection(editText.length())
                    }
                }
            } else {
                if (s?.contains(".") == true) {
                    editText.setText(s.substring(0,s.indexOf(".")))
                    editText.setSelection(editText.length())
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

    }

    override fun onDestroy() {
        super.onDestroy()
        if (socket.isConnected) {
            var c = Connect()
            c.sockect(socket)
            c.closeIOAndSocket()
        }
    }

    override fun onStop() {
        super.onStop()
        saveData()
    }
}