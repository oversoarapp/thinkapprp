package tw.com.thinkPower.ThinkAPPrp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_new_step.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.from
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processId
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList

class NewStepFragment : Fragment() {

    private var fragmentA = StepMainChildFragment()
    private var fragmentB = StepCondiChildFragment()

    override fun onCreateView (
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.fragment_new_step, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        if (from == "old" || from == "open") {
            newProcessTitle.text = processList[processId].stepName
        } else {
            var len = processList.size.toString().length
            var zero = ""
            if (len < 4) {
                for (i in len until 4) {
                    zero += "0"
                }
            }
            newProcessTitle.text = "STEP$zero${processList.size}"
        }

        if (GlobalVar.ip.isNotEmpty()) {
            imageView12.setImageResource(R.drawable.ic_signal_green)
        }

        val manager = childFragmentManager
        val transaction = manager.beginTransaction()
        transaction.add(R.id.childFrgment, fragmentA)
        transaction.commit()

        newStepBack.setOnClickListener {
            val manager = activity?.supportFragmentManager
            val transaction = manager?.beginTransaction()
            transaction?.replace(R.id.fragment_container, StepFragment())
            transaction?.commit()
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        val manager = childFragmentManager
                        val transaction = manager?.beginTransaction()
                        childFragmentManager.executePendingTransactions()
                        if (fragmentA.isAdded) {
                            transaction.show(fragmentA)
                        } else {
                            transaction.add(R.id.childFrgment, fragmentA)
                        }
                        if (fragmentB.isAdded) {
                            transaction.hide(fragmentB)
                        }
                        transaction.commit()
                    }
                    1 -> {
                        val manager = childFragmentManager
                        val transaction = manager.beginTransaction()
                        childFragmentManager.executePendingTransactions()
                        if (fragmentB.isAdded) {
                            transaction.show(fragmentB)
                        } else {
                            transaction.add(R.id.childFrgment, fragmentB)
                        }
                        if (fragmentA.isAdded) {
                            transaction.hide(fragmentA)
                        }
                        transaction.commit()
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })
    }
}
