package tw.com.thinkPower.ThinkAPPrp

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.implments.SwipeItemMangerImpl
import kotlinx.android.synthetic.main.fragment_saveprocess.*
import tw.com.thinkPower.ThinkAPPrp.Adapter.PstartAdapter
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.from
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList
import tw.com.thinkPower.ThinkAPPrp.Model.ProcessDataModel
import java.io.IOException
import java.math.BigDecimal
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException

class SaveProcessFragment :Fragment() {

    private var flag = 1
    private var machine = ""
    private var oriProcess = mutableListOf<ProcessDataModel>()
    private var nAdapter: PstartAdapter? = null
    private var command = "0000000FProcessFILEREAD"
    private var socket = Socket()
    private var mHandler = Handler()
    private var processNameList = mutableListOf<String>()

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_saveprocess, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        var bundle = this.arguments
        if (bundle != null) {
            machine = bundle.getString("Machine", "")
        }

        if (GlobalVar.ip.isNotEmpty()) {
            connectTask()
        }

        val click: (String) -> Unit = {
            saveProcessName.setText(it)
        }

        val deleteClick:(String, Int, SwipeLayout, SwipeItemMangerImpl) -> Unit = { name, pos, layout, manger ->
        }

        saveRv.layoutManager = LinearLayoutManager(context)
        nAdapter = PstartAdapter(
            context!!,
            false,
            click,
            deleteClick
        )
        saveRv.adapter = nAdapter

        saveBack.setOnClickListener {
            if (saveProcessName.text.toString() != "") {
                var isAvailable = false
                for (i in processNameList.indices) {
                    if (saveProcessName.text.toString() == processNameList[i]) {
                        isAvailable = true
                        break
                    }
                }
                if (!isAvailable) {
                    saveData()
                } else {
                    AlertDialog.Builder(context)
                        .setMessage("製程名稱重複，請問是否覆蓋原製程？")
                        .setNegativeButton("確定") { d, w ->
                            saveData()
                        }
                        .setPositiveButton("取消") { d, w ->
                            d.cancel()
                        }
                        .show()
                }
            } else {
                AlertDialog.Builder(context)
                    .setMessage("製程名稱空白，無法儲存，是否放棄儲存離開？")
                    .setNegativeButton("確定") { d, w ->
                        val manager = activity?.supportFragmentManager
                        val transaction = manager?.beginTransaction()
                        transaction?.replace(R.id.fragment_container, EditFragment())
                        transaction?.commit()
                    }
                    .setPositiveButton("取消") { d, w ->
                        d.cancel()
                    }
                    .show()
            }
        }

        saveProcessRefresh.setOnClickListener {
            if (ip.isNotEmpty()) {
                command = "0000000FProcessFILEREAD"
                connectTask()
            }
        }

    }

    private fun saveData() {

        processList.forEach {
            oriProcess.add(it.copy())
        }
        //在檢查一次loopend step
        processList.forEach {
            if(it.Function == "LoopEND1") {
                processList.mapIndexed { index,model ->
                    if (model.Function == "LoopST1") {
                        it.LoopEND1 = processList[index+1].stepName?.replace("STEP","")?.toInt()?.toString()
                    } else {
                        it.LoopEND1 = "1"
                    }
                }
            }
            if(it.Function == "LoopEND2") {
                processList.mapIndexed { index,model ->
                    if (model.Function == "LoopST2") {
                        it.LoopEND2 = processList[index+1].stepName?.replace("STEP","")?.toInt()?.toString()
                    } else {
                        it.LoopEND2 = "1"
                    }
                }
            }
        }

        convert()
        var tmp = ""
        for (i in 0 until processList.size) {
            if (i+1 == processList.size) {
                tmp += "\"Function-${i + 1}\":\"${processList[i].Function}\",\"LoopST1-${i + 1}\":${processList[i].LoopST1},\"LoopST2-${i + 1}\":${processList[i].LoopST2},\"LoopEND1-${i + 1}\":${processList[i].LoopEND1},\"LoopEND2-${i + 1}\":${processList[i].LoopEND2},\"Jump-${i + 1}\":${changeToStep(i,
                    processList[i].Jump ?: ""
                )},\"STEPAction-${i + 1}\":\"${processList[i].STEPAction}\",\"STEPModel-${i + 1}\":\"${processList[i].STEPModel}\",\"Vset-${i + 1}\":${processList[i].Vset},\"Iset-${i + 1}\":${processList[i].Iset},\"Pset-${i + 1}\":${processList[i].Pset},\"Timeset-${i + 1}\":${processList[i].Timeset},\"RecordTimeset-${i + 1}\":${processList[i].RecordTimeset},\"mAhset-${i + 1}\":${processList[i].mAhset},\"mAhCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].mAhCdtset ?: ""
                )},\"Whset-${i + 1}\":${processList[i].Whset},\"WhCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].WhCdtset ?: ""
                )},\"EVset-${i + 1}\":${processList[i].EVset},\"EVCompset-${i + 1}\":\"${processList[i].EVCompset}\",\"EVCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].EVCdtset ?: ""
                )},\"ECset-${i + 1}\":${processList[i].ECset},\"ECCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].ECCdtset ?: ""
                )},\"mdVset-${i + 1}\":${processList[i].mdVset},\"mdVCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].mdVCdtset ?: ""
                )},\"ETset-${i + 1}\":${processList[i].ETset},\"ETCompset-${i + 1}\":\"${processList[i].ETCompset}\",\"ETCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].ETCdtset ?: ""
                )},\"PULSEset-${i + 1}\":\"${processList[i].PULSEset}\""
            } else {
                tmp += "\"Function-${i + 1}\":\"${processList[i].Function}\",\"LoopST1-${i + 1}\":${processList[i].LoopST1},\"LoopST2-${i + 1}\":${processList[i].LoopST2},\"LoopEND1-${i + 1}\":${processList[i].LoopEND1},\"LoopEND2-${i + 1}\":${processList[i].LoopEND2},\"Jump-${i + 1}\":${changeToStep(i,
                    processList[i].Jump ?: ""
                )},\"STEPAction-${i + 1}\":\"${processList[i].STEPAction}\",\"STEPModel-${i + 1}\":\"${processList[i].STEPModel}\",\"Vset-${i + 1}\":${processList[i].Vset},\"Iset-${i + 1}\":${processList[i].Iset},\"Pset-${i + 1}\":${processList[i].Pset},\"Timeset-${i + 1}\":${processList[i].Timeset},\"RecordTimeset-${i + 1}\":${processList[i].RecordTimeset},\"mAhset-${i + 1}\":${processList[i].mAhset},\"mAhCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].mAhCdtset ?: ""
                )},\"Whset-${i + 1}\":${processList[i].Whset},\"WhCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].WhCdtset ?: ""
                )},\"EVset-${i + 1}\":${processList[i].EVset},\"EVCompset-${i + 1}\":\"${processList[i].EVCompset}\",\"EVCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].EVCdtset ?: ""
                )},\"ECset-${i + 1}\":${processList[i].ECset},\"ECCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].ECCdtset ?: ""
                )},\"mdVset-${i + 1}\":${processList[i].mdVset},\"mdVCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].mdVCdtset ?: ""
                )},\"ETset-${i + 1}\":${processList[i].ETset},\"ETCompset-${i + 1}\":\"${processList[i].ETCompset}\",\"ETCdtset-${i + 1}\":${changeToStep(i,
                    processList[i].ETCdtset ?: ""
                )},\"PULSEset-${i + 1}\":\"${processList[i].PULSEset}\","
            }
        }
        var step = "FILESEND,ProcessPath/${saveProcessName.text},{\"Machine\":\"$machine\",\"ProcessName\":\"${saveProcessName.text}.json\",$tmp}"
        var zero = ""
        var hex = Integer.toHexString(step.length)
        var size = 8 - hex.length
        for (i in 0 until size) {
            zero += "0"
        }
        command = zero+hex+step
        Log.d("saveProcess",command)
        if (ip.isNotEmpty()) {
            connectTask()
        }
    }

    private fun changeToStep(position:Int,id:String):Int{
        if (id != "0") {
            if (!id.contains("STEP")) {
                var mId = id
                return if (id.contains("Next")) {
                    //next存的是step
                    mId = mId.replace("Next", "")
                    mId.toInt()
                } else {
                    //step or end
                    var step = 0
                    processList.forEach {
                        if (it.id == mId) {
                            step = it.stepName?.replace("STEP", "")?.toInt() ?: 0
                        }
                    }
                    step
                }
            } else
            //沒找到要存的step，則改為next step
             return processList[position+1].stepName?.replace("STEP", "")?.toInt() ?: 0
        }
        return id.toInt()
    }

    private fun convertToServer(text:String):String {
        return if (text != "0" && text != "0.0") {
            (text.toDouble()*1000).toString()
        } else {
            text
        }
    }

    private fun convert() {
        for (i in 0 until processList.size) {
            processList[i].Vset = convertToServer(processList[i].Vset?:"")
            processList[i].Iset = convertToServer(processList[i].Iset?:"")
            processList[i].Timeset = convertToServer(processList[i].Timeset?:"")
            processList[i].RecordTimeset = convertToServer(processList[i].RecordTimeset?:"")
            processList[i].EVset = convertToServer(processList[i].EVset?:"")
            processList[i].ECset = convertToServer(processList[i].ECset?:"")
            processList[i].mdVset = convertToServer(processList[i].mdVset?:"")
        }
    }

    private val reConnectToast = Runnable {
        if (saveIPStatus != null) {
            saveIPStatus.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (progressBar7 != null) {
            progressBar7.visibility = View.INVISIBLE
            GlobalVar.ip = ""
            saveIPStatus.setImageResource(R.drawable.ic_signal_gray)
            Toast.makeText(context, "連線錯誤，請確認網路正常，重新設置ip！", Toast.LENGTH_SHORT).show()
        }
    }

    private fun connectTask () {
        val t = Thread(readData)
        t.start()
    }

    private fun sendTask() {
        var t = Thread(sendCommand)
        t.start()
    }

    private val readData = Runnable {
        try {
            mHandler.post{
                if (progressBar7 != null) {
                    progressBar7.visibility = View.VISIBLE
                }
            }
            socket = Socket()
            socket.connect(InetSocketAddress (GlobalVar.ip, 1688),3000)

            if (socket.isConnected) {
                mHandler.post {
                    if (progressBar7 != null) {
                        saveIPStatus.setImageResource(R.drawable.ic_signal_green)
                        progressBar7.visibility = View.INVISIBLE
                        sendTask()
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable {
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand(command)
        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler.post(reConnectToast)
            } else {
                flag = 1
                mHandler.post(connectError)
            }
        }

        if (msg.isEmpty() || msg.contains("failed") || msg.contains("00000058The file path does not exist, or the file format, or the process file name is incorrect.")||msg.contains("error")) {
            if(msg.contains("00000058The file path does not exist, or the file format, or the process file name is incorrect.")) {
                msg = msg.substring(8)
            }
            if (command.contains("FILESEND")) {
                mHandler.post {
                    if (progressBar7 != null) {
                        progressBar7.visibility = View.INVISIBLE
                        AlertDialog.Builder(context)
                            .setMessage(msg)
                            .setPositiveButton("確定") { d,w ->
                                from = "old"
                                processList = oriProcess
                                val manager = activity?.supportFragmentManager
                                val transaction = manager?.beginTransaction()
                                transaction?.replace(R.id.fragment_container, StepFragment())
                                transaction?.commit()
                            }
                            .show()
                    }
                }
            } else {
                mHandler.post {
                    if (progressBar7 != null) {
                        progressBar7.visibility = View.INVISIBLE
                        AlertDialog.Builder(context)
                            .setMessage(msg)
                            .show()
                    }
                }
            }
        } else {
            Log.d("msg",msg)
            if (command.contains("ProcessFILEREAD")) {
                //Process List
                processNameList = msg.substring(8).replace(".json", "").split(",").toMutableList()
                mHandler.post {
                    if (progressBar7 != null) {
                        progressBar7.visibility = View.INVISIBLE
                        nAdapter?.updateData(processNameList.sorted().toMutableList())
                    }
                }
            } else if (command.contains("FILESEND")){
                processList = mutableListOf(ProcessDataModel("","","1","STEP0001","END","0","0","0","0","0","","","0","0","0", "0","0","0","","0","","0","0","","0"))
                mHandler.post {
                    Toast.makeText(context,"OK",Toast.LENGTH_SHORT).show()
                    val manager = activity?.supportFragmentManager
                    val transaction = manager?.beginTransaction()
                    transaction?.replace(R.id.fragment_container, EditFragment())
                    transaction?.commit()
                }
            }
        }
    }
}