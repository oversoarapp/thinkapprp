package tw.com.thinkPower.ThinkAPPrp

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.child_fragment_connect.*
import tw.com.thinkPower.ThinkAPPrp.Adapter.ShowIpAdapter
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import java.io.IOException
import java.net.*

class ConnectChildFragment : Fragment() {

    private var flag = 1
    private var socket = Socket()
    private var nAdapter: ShowIpAdapter? = null
    private var mHandler:Handler? = null
    private var sp:SharedPreferences? = null
    private var t:Thread? = null
    private var ipList = mutableListOf<String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(context).inflate(R.layout.child_fragment_connect, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        mHandler = Handler()

        if (ip.isNotEmpty()) {
            setDone.isChecked = true
            setIp.setText(ip)
            setIp.isEnabled = false
            setIp.background = context?.getDrawable(R.color.colorGrey)
            setIPstatus.setImageResource(R.drawable.ic_signal_green)
        }

        sp = activity?.getSharedPreferences("set", Context.MODE_PRIVATE)

        val click:(String) -> Unit = {
            if (ip.isEmpty()) {
                setIp.isEnabled = true
                setIp.background = context?.getDrawable(R.color.colorWhite)
                setIp.setText(it)
            }
        }

        val delete:(Int) -> Unit = {
            sp?.edit()?.remove("ip$it")?.apply()
            ipList = nAdapter?.dataList!!
        }

        nAdapter =
            ShowIpAdapter(context!!,click, delete)
        setRv.adapter = nAdapter
        setRv.layoutManager = LinearLayoutManager(context)

        var n = 0
        while (sp?.getString("ip$n", "")?.isEmpty() == false) {
            if (sp?.getString("ip$n", "")?.isEmpty() == true) {
                break
            }
            ipList.add(sp?.getString("ip$n", "") ?: "")
            n++
        }
        nAdapter?.updateData(ipList)

        setDone.setOnCheckedChangeListener { buttonView, isChecked ->
            if (setIp.text.isNotEmpty()) {
                if (isChecked) {
                    if (ipList.size != 0) {
                        if (!ipList.contains(setIp.text.toString())) {
                            for (i in 0..ipList.size) {
                                if (sp?.getString("ip$i", "")?.isEmpty() == true) {
                                    sp?.edit()?.putString("ip$i", setIp.text.toString())?.apply()
                                }
                            }
                            ipList.add(setIp.text.toString())
                            nAdapter?.updateData(ipList)
                        }
                    } else {
                        ipList.add(setIp.text.toString())
                        sp?.edit()?.putString("ip0", setIp.text.toString())?.apply()
                        nAdapter?.updateData(ipList)
                    }
                    if (progressBar != null) {
                        progressBar.visibility = View.VISIBLE
                        connectTask()
                    }
                } else {
                    if(progressBar != null && progressBar.visibility == View.VISIBLE) {
                        progressBar.visibility = View.INVISIBLE
                    }
                    mHandler = null
                    ip = ""
                    if (setIPstatus != null) {
                        setIp.isEnabled = true
                        setIp.background = context?.getDrawable(R.color.colorWhite)
                        setIPstatus?.setImageResource(R.drawable.ic_signal_gray)
                    }
                }
            } else {
                setDone.isChecked = false
                Toast.makeText(context,"請輸入IP位置！",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val reConnect = Runnable {
        connectTask()
        socket = Socket()
        if (setIPstatus != null) {
            setIPstatus.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (progressBar != null) {
            progressBar.visibility = View.INVISIBLE
            setDone.isChecked = false
            Toast.makeText(context, "連線錯誤，請確認IP位置！", Toast.LENGTH_SHORT).show()
            setIPstatus.setImageResource(R.drawable.ic_signal_gray)
        }
    }

    private fun connectTask () {
        mHandler = Handler()
        t = Thread(readData)
        t?.start()
    }

    private val readData = Runnable {
        try {
            socket.connect(InetSocketAddress (setIp.text.toString(), 1688),3000)

            if (socket.isConnected) {
                mHandler?.post {
                    if (progressBar != null) {
                        progressBar?.visibility = View.INVISIBLE
                        setIPstatus?.setImageResource(R.drawable.ic_signal_green)
                        setIp.isEnabled = false
                        setIp.background = context?.getDrawable(R.color.colorGrey)
                        ip = setIp.text.toString()
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <= 2) {
                flag++
                mHandler?.post(reConnect)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }  catch (x:UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                mHandler?.post(reConnect)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mHandler = null
        t = null
    }

    override fun onDestroy() {
        super.onDestroy()
        if (socket.isConnected) {
            var c = Connect()
            c.sockect(socket)
            c.closeIOAndSocket()
        }
    }
}