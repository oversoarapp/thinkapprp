package tw.com.thinkPower.ThinkAPPrp

import StepListAdapter
import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_edit_new.*
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.from
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.ip
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processId
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.processList
import tw.com.thinkPower.ThinkAPPrp.GlobalVar.scrollToPosition
import tw.com.thinkPower.ThinkAPPrp.Model.InfoModel
import tw.com.thinkPower.ThinkAPPrp.Model.ProcessDataModel
import java.io.IOException
import java.math.BigDecimal
import java.net.InetSocketAddress
import java.net.Socket
import java.net.UnknownHostException


class StepFragment: Fragment() {

    private var socket = Socket()
    private var flag = 1
    private var mHandler:Handler? = null
    private var nAdapter: StepListAdapter? = null
    private var choosePosition = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

    }

    private fun initView() {

        if (ip.isNotEmpty()) {
            imageView7.setImageResource(R.drawable.ic_signal_green)
        }

        var bundle = this.arguments
        if (bundle?.getString("ProcessData")?.isNotEmpty() == true) {
            convertData(bundle.getString("ProcessData")?:"")
        }

        mHandler = Handler()

        val click:(Int)->Unit = {
            if (from != "open") {
                from = "old"
            }
            processId = it
            scrollToPosition = it
            val manager = activity?.supportFragmentManager
            val transaction = manager?.beginTransaction()
            val fragment = NewStepFragment()
            transaction?.replace(R.id.fragment_container,fragment)
            transaction?.commit()
        }

        val choose:(String)->Unit = {
            choosePosition = it
        }

        newProcessList.layoutManager = LinearLayoutManager(context)
        nAdapter = StepListAdapter(context!!,processList,click,choose)
        newProcessList.adapter = nAdapter

        if (scrollToPosition > 0) {
            newProcessList.scrollToPosition(scrollToPosition)
            scrollToPosition = 0
        }

        newBackBtn.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("訊息提示")
                .setMessage("提醒您，您尚未儲存製程，是否放棄儲存直接離開？")
                .setNegativeButton("確定") { d,w ->
                    val manager = activity?.supportFragmentManager
                    val transaction = manager?.beginTransaction()
                    transaction?.replace(R.id.fragment_container, EditFragment())
                    transaction?.commit()
                }
                .setPositiveButton("取消") {d,w ->
                    d.cancel()
                }
                .show()
        }

        stepUpBtn.setOnClickListener {
            stepUpOrDown("up")
        }

        stepDownBtn.setOnClickListener {
            stepUpOrDown("down")
        }

        newProcessBtn.setOnClickListener {
            from = "new"
            var id = processList.size+1
            for (i in processList.indices) {
                if (processList[i].id == (processList.size+1).toString()) {
                    id = processList.size+2
                }
            }
            processId = id
            val manager = activity?.supportFragmentManager
            val transaction = manager?.beginTransaction()
            val fragment = NewStepFragment()
            transaction?.replace(R.id.fragment_container,fragment)
            transaction?.commit()
        }

        saveBtn.setOnClickListener {
            if(ip.isNotEmpty() && processList.size > 1) {
                AlertDialog.Builder(context)
                    .setTitle("訊息提示")
                    .setMessage("編輯是否確定結束，將前往儲存頁！")
                    .setCancelable(false)
                    .setNegativeButton("確定") {d,w ->
                        connectTask()
                    }
                    .setPositiveButton("取消") {d,w ->
                        d.cancel()
                    }
                    .show()
            }
        }

    }

    private val reConnectToast = Runnable {
        if(imageView7 != null) {
            imageView7.setImageResource(R.drawable.ic_signal_y)
        }
    }

    private val connectError = Runnable {
        if (progressBar8 != null) {
            progressBar8.visibility = View.INVISIBLE
            ip = ""
            imageView7.setImageResource(R.drawable.ic_signal_gray)
            Toast.makeText(context, "連線錯誤，請確認網路正常，重新設置ip！", Toast.LENGTH_SHORT).show()
        }
    }

    private fun connectTask () {
        val t = Thread(readData)
        t.start()
    }

    private fun sendTask() {
        var t = Thread(sendCommand)
        t.start()
    }

    private val readData = Runnable {
        try {
            mHandler?.post {
                if (progressBar8 != null) {
                    progressBar8.visibility = View.VISIBLE
                }
            }
            socket.connect(InetSocketAddress (GlobalVar.ip, 1688),3000)

            if (socket.isConnected) {
                sendTask()
            }

        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }  catch (x: UnknownHostException) {
            x.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }
    }

    private val sendCommand = Runnable{
        var msg = ""
        try {
            var connect = Connect()
            connect.sockect(socket)
            msg = connect.sendCommand("00000017FILEREAD,DeviceINFOPath")
        } catch (e: IOException) {
            e.printStackTrace()
            if (flag <=2) {
                flag++
                connectTask()
                mHandler?.post(reConnectToast)
            } else {
                flag = 1
                mHandler?.post(connectError)
            }
        }

        if (msg != "" || !msg.contains("failed") || !msg.contains("ERROR")) {
            mHandler?.post {
                if (progressBar8 != null && msg.contains("{")) {
                    progressBar8.visibility = View.INVISIBLE
                    val response = msg.substring(msg.indexOf("{"))
                    val listType = object : TypeToken<InfoModel>() {}.type
                    val infoModel: InfoModel = Gson().fromJson(response, listType)
                    var fragment = SaveProcessFragment()
                    var bundle = Bundle()
                    bundle.putString("Machine", infoModel.MachineModel)
                    fragment.arguments = bundle

                    val manager = activity?.supportFragmentManager
                    val transaction = manager?.beginTransaction()
                    transaction?.replace(R.id.fragment_container, fragment)
                    transaction?.commit()
                }
            }
        } else {
            mHandler?.post{
                if (progressBar8 != null) {
                    AlertDialog.Builder(context)
                        .setMessage(msg)
                        .show()
                    progressBar8.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun newStepName():String{
        var endPos:Int? = null
        processList.mapIndexed { index, processModel ->
            if (processModel.Function == "END") {
                endPos = index
            }
        }
        if (endPos != null) {
            var len = (endPos!!+1).toString().length
            var zero = ""
            if (len < 4) {
                for (i in len until 4) {
                    zero += "0"
                }
            }
            return "STEP$zero${endPos!!+1}"
        } else {
            var len = (processList.size+1).toString().length
            var zero = ""
            if (len < 4) {
                for (i in len until 4) {
                    zero += "0"
                }
            }
            return "STEP$zero${processList.size+1}"
        }
    }

    private fun convertData(data:String) {
        var newList = mutableListOf<String>()
        var subStr = data.substring(data.indexOf("\"Function-1\":"),data.length-1).replace("\n","").replace("\t","").replace("}","")
        var splitList = subStr.split(",").toMutableList()
        var step = ""
//        var mList = splitList
        var number = splitList[0].substring(splitList[0].indexOf("-")+1,splitList[0].indexOf("-")+2)
        for (x in 0 until splitList.size) {
            var n = splitList[x].substring(0,splitList[x].indexOf(":")).replace("\"","")
            var mNumber = (n.last()).toString()
            if (number != mNumber) {
                newList.add("{${step.substring(0, step.length-1)}}")
                step = ""
                number = mNumber
            }
            if (!splitList[x].contains("Label")) {
                step += "${splitList[x]},"
            }
            if (x == splitList.size -1 ) {
                newList.add("{${step.substring(0, step.length-1)}}")
            }
        }
        Log.i("originalList",newList.toString())

        for(i in newList.indices) {
            for (x in 0 .. newList.size) {
                if (i >= 9) {
                    if (newList[i].contains("-${x+10}")) {
                        newList[i] = newList[i].replace("-${x+10}", "")
                        break
                    }
                } else {
                    if (newList[i].contains("-$x")) {
                        newList[i] = newList[i].replace("-$x", "")
                        break
                    }
                }
            }
        }

        val listType = object:TypeToken<MutableList<ProcessDataModel>>(){}.type
        val dataList:MutableList<ProcessDataModel> = Gson().fromJson(newList.toString(),listType)
        processList = dataList
        processList.mapIndexed { index, processDataModel ->
            var zero = ""
            var size = 4-(index+1).toString().length
            for (i in 0 until size) {
                zero += "0"
            }
            processDataModel.stepName = "STEP$zero${index+1}"
            processDataModel.id = "${index+1}"
            //step 轉 id
            if (processDataModel.Jump != "0") {
                var size = 4 - (processDataModel.Jump?.length?:0)
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var jumpTo = "STEP$zero${processDataModel.Jump}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == jumpTo) {
                        processDataModel.Jump = it.id
                    }
                }
            }
            if (processDataModel.LoopEND1 != "0") {
                var size = 4 - (processDataModel.LoopEND1?.length?:0)
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var loopEND1 = "STEP$zero${processDataModel.LoopEND1}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == loopEND1) {
                        processDataModel.LoopEND1 = it.id
                    }
                }
            }
            if (processDataModel.LoopEND2 != "0") {
                var size = 4 - (processDataModel.LoopEND2?.toInt()).toString().length
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var loopEND2 = "STEP$zero${processDataModel.LoopEND2}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == loopEND2) {
                        processDataModel.LoopEND2 = it.id
                    }
                }
            }
            if (processDataModel.ECCdtset != "0") {
                var size = 4 - (processDataModel.ECCdtset?.toInt()).toString().length
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var eCCdtset = "STEP$zero${processDataModel.ECCdtset}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == eCCdtset) {
                        processDataModel.ECCdtset = it.id
                    }
                }
            }
            if (processDataModel.mAhCdtset != "0") {
                var size = 4 - (processDataModel.mAhCdtset?.toInt()).toString().length
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var mAhCdtset = "STEP$zero${processDataModel.mAhCdtset}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == mAhCdtset) {
                        processDataModel.mAhCdtset = it.id
                    }
                }
            }
            if (processDataModel.WhCdtset != "0") {
                var size = 4 - (processDataModel.WhCdtset?.toInt()).toString().length
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var whCdtset = "STEP$zero${processDataModel.WhCdtset}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == whCdtset) {
                        processDataModel.WhCdtset = it.id
                    }
                }
            }
            if (processDataModel.EVCdtset != "0") {
                var size = 4 - (processDataModel.EVCdtset?.toInt()).toString().length
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var eVCdtset = "STEP$zero${processDataModel.EVCdtset}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == eVCdtset) {
                        processDataModel.EVCdtset = it.id
                    }
                }
            }
            if (processDataModel.mdVCdtset != "0") {
                var size = 4 - (processDataModel.mdVCdtset?.toInt()).toString().length
                var zero = ""
                for (i in 0 until size) {
                    zero += "0"
                }
                var mdVCdtset = "STEP$zero${processDataModel.mdVCdtset}"
                processList.mapIndexed { index, it ->
                    if (it.stepName == mdVCdtset) {
                        processDataModel.mdVCdtset = it.id
                    }
                }
            }
        }
        for ( i in processList.indices) {
            processList[i].Iset = convertBack( processList[i].Iset ?: "")
            processList[i].Vset = convertBack( processList[i].Vset ?: "")
            processList[i].Timeset = convertBack( processList[i].Timeset ?: "")
            processList[i].RecordTimeset = convertBack( processList[i].RecordTimeset ?: "")
            processList[i].EVset = convertBack(processList[i].EVset?:"")
            processList[i].ECset = convertBack(processList[i].ECset?:"")
            processList[i].mdVset = convertBack(processList[i].mdVset?:"")
        }
        nAdapter?.updateData(processList)
        Log.i("convertList", processList.toString())
    }

    private fun stepUpOrDown (action:String) {
        when(action) {
            "up" -> {
                if (choosePosition != "") {
                    processList.mapIndexed { index, processDataModel ->
                        if(processDataModel.id == choosePosition) {
                            if (index != 0 && processDataModel.Function != "END") {
                                //不是第一個也不是STEP END
                                var tmp = processList[index]
                                var chooseName = tmp.stepName
                                var name2 = processList[index - 1].stepName
                                processList[index] = processList[index - 1]
                                processList[index - 1] = tmp
                                processList[index].stepName = chooseName
                                processList[index - 1].stepName = name2
                            }
                        }
                    }
                }
            }
            "down" -> {
                if (choosePosition != "") {
                    for (i in processList.indices) {
                        if(processList[i].id == choosePosition) {
                            if (processList[i].Function != "END" && processList[i+1].Function != "END") {
                                //不是STEP END、下一個也不是END
                                var tmp = processList[i]
                                var chooseName = tmp.stepName
                                var name2 = processList[i + 1].stepName
                                processList[i] = processList[i + 1]
                                processList[i + 1] = tmp
                                processList[i].stepName = chooseName
                                processList[i + 1].stepName = name2
                                break
                            }
                        }
                    }
                }
            }
        }
        nAdapter?.updateData(processList)
    }

    private fun convertBack(text:String):String {
        return if (text != "0" && text != "0.0" && text.isNotEmpty()) {
            var m = (text.toDouble() / 1000).toString()
            if (m.substring(m.indexOf(".") + 1) == "0" && m.substring(
                    0,
                    m.indexOf(".") + 2
                ).length == m.length
            ) {
                val bd = BigDecimal(m)
                bd.setScale(0, 1).toString()
            } else {
                m
            }
        } else {
            text
        }
        return text
    }
}