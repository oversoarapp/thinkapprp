package tw.com.thinkPower.ThinkAPPrp.Model

data class SetInfoModel(
    val AlarmDelay: Int? = null,
    val COCP: Int? = null,
    val CUCP: Int? = null,
    val ChDchVFBSW: String? = null,
    val DOCP: Int? = null,
    val DUCP: Int? = null,
    val ECDelay: Int? = null,
    val EVDelay: Int? = null,
    val OTP: Double? = null,
    val OVP: Int? = null,
    val UVP: Int? = null
)