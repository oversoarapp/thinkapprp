package tw.com.thinkPower.ThinkAPPrp.Model

data class InfoModel(
    val CalibrationDate: String,
    val FWversion: String,
    val HWversion: String,
    val MachineModel: String,
    val ManufactureDate: String,
    val SeriesNumber: String
)